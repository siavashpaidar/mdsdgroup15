package se.chalmers.cse.mdsd1617.yakindu;

public class Room {
	private long roomId;
	private RoomStatus status;
	
	public enum RoomStatus {
		FREE,OCCUPIED,ASSIGNED
	}
	
	public Room(long roomId){
		this.roomId = roomId;
		status = RoomStatus.FREE;
	}
	
	public long getRoomId(){
		return roomId;
	}
	
	public RoomStatus getStatus(){
		return status;
	}
	
	public void setRoomStatus(RoomStatus status){
		this.status = status;
	}
	
	public boolean isFree(){
		return status.equals(RoomStatus.FREE);
	}
	
	public boolean isOccupied(){
		return status.equals(RoomStatus.OCCUPIED);
	}
	
	public boolean isAssigned(){
		return status.equals(RoomStatus.ASSIGNED);
	}
}
