package se.chalmers.cse.mdsd1617.yakindu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.chalmers.cse.mdsd1617.yakindu.Reservation.ReservationStatus;
import se.chalmers.cse.mdsd1617.yakindu.Room.RoomStatus;

public class Assignment3Complete {
	private long currentBookingId = 0;
	private long currentReservationNumber = 0;
	private Map<Long, List<Reservation>> map = new HashMap<>();	//Maps a booking to a list of reservations
	private Map<Long, Long> bookingIds = new HashMap<>();
	private List<Room> rooms = new ArrayList<>();
	private final int NUMBER_OF_ROOMS = 2;
	private final int MAX_ROOMS = 2;

	/**
	 * Initiates a booking.
	 * Increments the current booking ID.
	 * @return the current booking ID
	 */
	public long initiateBooking(long selectedBooking) {
		if(rooms.isEmpty()){
			initRooms(NUMBER_OF_ROOMS);	//Creates two rooms
		}
		++currentBookingId;
		List<Reservation> reservations = new ArrayList<>();
		bookingIds.put(selectedBooking, currentBookingId);
		map.put(currentBookingId, reservations); //Add the current booking to the map
		return currentBookingId;
	}

	/**
	 * Adds a room (room type) to a booking.
	 * @param bookingId The booking to add the room to
	 * @return True if a free room exists, false otherwise
	 */
	public boolean addRoomToBooking(long bookingId) {
		if (bookingId < 1 || bookingId > currentBookingId) {
			return false;
		} else if (currentReservationNumber >= MAX_ROOMS) {
			return false;
		} else {
			
			//Finds the first free room, and changes the status to assigned.
			//Also adds that room to the reservation, and maps the reservation to the booking.
			for(Room room : rooms){
				if(room.getStatus().equals(RoomStatus.FREE)){
					++currentReservationNumber;
					room.setRoomStatus(RoomStatus.ASSIGNED);
					Reservation reservation = new Reservation(currentReservationNumber);
					reservation.setRoom(room);
					map.get(bookingId).add(reservation);
					return true;
				}
			}
			
			return false;
		}
	}
	
	/**
	 * Confirms a booking.
	 * Changes the status of the reservations related to the booking to confirmed.
	 * @param bookingId The booking to confirm
	 * @return True if reservations have been confirmed successfully
	 */
	public boolean confirmBooking(long bookingId){
		List<Reservation> reservations = map.get(bookingId);
		if(reservations != null && !reservations.isEmpty()){
			for(Reservation reservation : reservations){
				reservation.setStatus(ReservationStatus.CONFIRMED);
			}
			return true;
		}
		return false;
	}
	
	public boolean checkInReservation(long bookingId, long reservationId){
		List<Reservation> reservations = map.get(bookingIds.get(bookingId));
		if(reservations != null && !reservations.isEmpty()){
			for(Reservation reservation : reservations){
				if(reservation.getReservationId()==reservationId){
					reservation.setStatus(ReservationStatus.CHECKED_IN);
					reservation.getRoom().setRoomStatus(RoomStatus.OCCUPIED);
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean checkOutReservation(long bookingId, long reservationId){
		List<Reservation> reservations = map.get(bookingIds.get(bookingId));
		if(reservations != null && !reservations.isEmpty()){
			for(Reservation reservation : reservations){
				if(reservation.getReservationId()==reservationId){
					reservation.setStatus(ReservationStatus.CHECKED_OUT);
					reservation.getRoom().setRoomStatus(RoomStatus.FREE);
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean allCheckedInOrOut(long bookingId){
		List<Reservation> reservations = map.get(bookingId);
		if(reservations != null && !reservations.isEmpty()){
			for(Reservation reservation : reservations){
				if(!reservation.isCheckedIn() && !reservation.isCheckedOut()){
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	public boolean allCheckedOut(long bookingId){
		List<Reservation> reservations = map.get(bookingId);
		if(reservations != null && !reservations.isEmpty()){
			for(Reservation reservation : reservations){
				if(!reservation.isCheckedOut()){
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Checks out a booking.
	 * Changes the status of the reservations related to the booking to checked out.
	 * Also updates the status of rooms related to the reservations to free.
	 * @param bookingId The booking to check out
	 * @return True if reservations have been checked out successfully
	 */
	public boolean initiateCheckout(long bookingId){
		List<Reservation> reservations = map.get(bookingId);
		if(reservations != null && !reservations.isEmpty()){
			for(Reservation reservation : reservations){
				reservation.setStatus(ReservationStatus.CHECKED_OUT);
				reservation.getRoom().setRoomStatus(RoomStatus.FREE);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Checks whether a reservation is confirmed or not.
	 * @param reservationId The reservation to check
	 * @return True if the reservation is confirmed, false otherwise
	 */
	public boolean reservationConfirmed(long reservationId){
		for(Map.Entry<Long, List<Reservation>> entry : map.entrySet()){
			for(Reservation reservation : entry.getValue()){
				if(reservation.getReservationId()==reservationId){
					return reservation.isConfirmed();
				}
			}
		}
		return false;
	}
	
	/**
	 * Checks whether a reservation is checked in or not.
	 * @param reservationId The reservation to check
	 * @return True if the reservation is checked in, false otherwise
	 */
	public boolean reservationCheckedIn(long reservationId){
		for(Map.Entry<Long, List<Reservation>> entry : map.entrySet()){
			for(Reservation reservation : entry.getValue()){
				if(reservation.getReservationId()==reservationId){
					return reservation.isCheckedIn();
				}
			}
		}
		return false;
	}
	
	/**
	 * Checks whether a reservation is checked out or not.
	 * @param reservationId The reservation to check
	 * @return True if the reservation is checked out, false otherwise
	 */
	public boolean reservationCheckedOut(long reservationId){
		for(Map.Entry<Long, List<Reservation>> entry : map.entrySet()){
			for(Reservation reservation : entry.getValue()){
				if(reservation.getReservationId()==reservationId){
					return reservation.isCheckedOut();
				}
			}
		}
		return false;
	}
	
	/**
	 * Checks whether a room is free or not.
	 * @param roomId The room to check
	 * @return True if the room is free, false otherwise
	 */
	public boolean roomFree(long roomId){
		if(rooms.isEmpty()){
			return true;
		}
		return rooms.get((int) roomId).isFree();
	}
	
	/**
	 * Checks whether a room is assigned or not.
	 * @param roomId The room to check
	 * @return True if the room is assigned, false otherwise
	 */
	public boolean roomAssigned(long roomId){
		if(rooms.isEmpty()){
			return false;
		}
		return rooms.get((int) roomId).isAssigned();
	}
	
	/**
	 * Checks whether a room is occupied or not.
	 * @param roomId The room to check
	 * @return True if the room is occupied, false otherwise
	 */
	public boolean roomOccupied(long roomId){
		if(rooms.isEmpty()){
			return false;
		}
		return rooms.get((int) roomId).isOccupied();
	}
	
	/**
	 * Creates a number of rooms.
	 * @param numberOfRooms The number of rooms to create
	 */
	private void initRooms(int numberOfRooms){
		for(int i=0; i<numberOfRooms; i++){
			rooms.add(new Room(i));
		}
	}
	
	/**
	 * @return The current reservation number
	 */
	public long getReservationNumber(){
		return currentReservationNumber;
	}
	
	//Interface method - Might be implemented
	public boolean payDuringCheckout(long bookingId){
		return true;
	}
}
