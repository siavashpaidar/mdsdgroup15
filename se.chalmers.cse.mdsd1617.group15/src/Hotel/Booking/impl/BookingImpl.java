/**
 */
package Hotel.Booking.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Map.Entry;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.InternalEList;

import Hotel.Booking.Booking;
import Hotel.Booking.BookingPackage;
import Hotel.Booking.BookingStatus;
import Hotel.Entities.Extra;
import Hotel.Room.Room;
import Hotel.RoomType.RoomType;
import Hotel.User.Customer.Customer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Booking.impl.BookingImpl#getBookingId <em>Booking Id</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingImpl#getCustomer <em>Customer</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingImpl#getBookingStatus <em>Booking Status</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingImpl#getExtraCosts <em>Extra Costs</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingImpl#getRoomTypeToInteger <em>Room Type To Integer</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingImpl#getFromDay <em>From Day</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingImpl#getToDay <em>To Day</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingImpl#getRooms <em>Rooms</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingImpl#getRoomExtras <em>Room Extras</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	/**
	 * The default value of the '{@link #getBookingId() <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingId()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKING_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBookingId() <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingId()
	 * @generated
	 * @ordered
	 */
	protected int bookingId = BOOKING_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCustomer() <em>Customer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomer()
	 * @generated
	 * @ordered
	 */
	protected Customer customer;

	/**
	 * The default value of the '{@link #getBookingStatus() <em>Booking Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingStatus()
	 * @generated
	 * @ordered
	 */
	protected static final BookingStatus BOOKING_STATUS_EDEFAULT = BookingStatus.CHECKED_IN;

	/**
	 * The cached value of the '{@link #getBookingStatus() <em>Booking Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingStatus()
	 * @generated
	 * @ordered
	 */
	protected BookingStatus bookingStatus = BOOKING_STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtraCosts() <em>Extra Costs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtraCosts()
	 * @generated
	 * @ordered
	 */
	protected static final double EXTRA_COSTS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getExtraCosts() <em>Extra Costs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtraCosts()
	 * @generated
	 * @ordered
	 */
	protected double extraCosts = EXTRA_COSTS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoomTypeToInteger() <em>Room Type To Integer</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeToInteger()
	 * @generated
	 * @ordered
	 */
	protected EMap<RoomType, Integer> roomTypeToInteger;

	/**
	 * The default value of the '{@link #getFromDay() <em>From Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromDay()
	 * @generated
	 * @ordered
	 */
	protected static final Date FROM_DAY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFromDay() <em>From Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromDay()
	 * @generated
	 * @ordered
	 */
	protected Date fromDay = FROM_DAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getToDay() <em>To Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToDay()
	 * @generated
	 * @ordered
	 */
	protected static final Date TO_DAY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToDay() <em>To Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToDay()
	 * @generated
	 * @ordered
	 */
	protected Date toDay = TO_DAY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * The cached value of the '{@link #getRoomExtras() <em>Room Extras</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomExtras()
	 * @generated
	 * @ordered
	 */
	protected EMap<Room, EList<Extra>> roomExtras;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingImpl() {
		super();
		roomTypeToInteger = new BasicEMap<RoomType,Integer>();
		rooms = new BasicEList<Room>();
		roomExtras = new BasicEMap<Room,EList<Extra>>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookingId() {
		return bookingId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingId(int newBookingId) {
		int oldBookingId = bookingId;
		bookingId = newBookingId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__BOOKING_ID, oldBookingId, bookingId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Customer getCustomer() {
		if (customer != null && customer.eIsProxy()) {
			InternalEObject oldCustomer = (InternalEObject)customer;
			customer = (Customer)eResolveProxy(oldCustomer);
			if (customer != oldCustomer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKING__CUSTOMER, oldCustomer, customer));
			}
		}
		return customer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Customer basicGetCustomer() {
		return customer;
	}
	
	/**
	 * 
	 * @param rooms
	 * 
	 * @generated NOT
	 */
	public void setRooms(EList<Room> rooms) {
		this.rooms = rooms;
	}
	
	/**
	 * 
	 * @param roomExtras
	 * 
	 * @generated NOT
	 */
	public void setExtras(EMap<Room, EList<Extra>> roomExtras) {
		this.roomExtras = roomExtras;
	}
	
	/**
	 * 
	 * @param roomTypeToInteger
	 * 
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public void setRoomTypeToInteger(Entry<RoomType, Integer> roomTypeToInteger) {
		this.roomTypeToInteger = (EMap<RoomType, Integer>) roomTypeToInteger;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCustomer(Customer newCustomer) {
		Customer oldCustomer = customer;
		customer = newCustomer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__CUSTOMER, oldCustomer, customer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingStatus getBookingStatus() {
		return bookingStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingStatus(BookingStatus newBookingStatus) {
		BookingStatus oldBookingStatus = bookingStatus;
		bookingStatus = newBookingStatus == null ? BOOKING_STATUS_EDEFAULT : newBookingStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__BOOKING_STATUS, oldBookingStatus, bookingStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getExtraCosts() {
		return extraCosts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtraCosts(double newExtraCosts) {
		double oldExtraCosts = extraCosts;
		extraCosts = newExtraCosts;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__EXTRA_COSTS, oldExtraCosts, extraCosts));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<RoomType, Integer> getRoomTypeToInteger() {
		return roomTypeToInteger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getFromDay() {
		return fromDay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromDay(Date newFromDay) {
		Date oldFromDay = fromDay;
		fromDay = newFromDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__FROM_DAY, oldFromDay, fromDay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getToDay() {
		return toDay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToDay(Date newToDay) {
		Date oldToDay = toDay;
		toDay = newToDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__TO_DAY, oldToDay, toDay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> getRooms() {
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<Room, EList<Extra>> getRoomExtras() {
		return roomExtras;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BookingPackage.BOOKING__ROOM_TYPE_TO_INTEGER:
				return ((InternalEList<?>)getRoomTypeToInteger()).basicRemove(otherEnd, msgs);
			case BookingPackage.BOOKING__ROOM_EXTRAS:
				return ((InternalEList<?>)getRoomExtras()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING__BOOKING_ID:
				return getBookingId();
			case BookingPackage.BOOKING__CUSTOMER:
				if (resolve) return getCustomer();
				return basicGetCustomer();
			case BookingPackage.BOOKING__BOOKING_STATUS:
				return getBookingStatus();
			case BookingPackage.BOOKING__EXTRA_COSTS:
				return getExtraCosts();
			case BookingPackage.BOOKING__ROOM_TYPE_TO_INTEGER:
				if (coreType) return getRoomTypeToInteger();
				else return getRoomTypeToInteger().map();
			case BookingPackage.BOOKING__FROM_DAY:
				return getFromDay();
			case BookingPackage.BOOKING__TO_DAY:
				return getToDay();
			case BookingPackage.BOOKING__ROOMS:
				return getRooms();
			case BookingPackage.BOOKING__ROOM_EXTRAS:
				if (coreType) return getRoomExtras();
				else return getRoomExtras().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING__BOOKING_ID:
				setBookingId((Integer)newValue);
				return;
			case BookingPackage.BOOKING__CUSTOMER:
				setCustomer((Customer)newValue);
				return;
			case BookingPackage.BOOKING__BOOKING_STATUS:
				setBookingStatus((BookingStatus)newValue);
				return;
			case BookingPackage.BOOKING__EXTRA_COSTS:
				setExtraCosts((Double)newValue);
				return;
			case BookingPackage.BOOKING__ROOM_TYPE_TO_INTEGER:
				((EStructuralFeature.Setting)getRoomTypeToInteger()).set(newValue);
				return;
			case BookingPackage.BOOKING__FROM_DAY:
				setFromDay((Date)newValue);
				return;
			case BookingPackage.BOOKING__TO_DAY:
				setToDay((Date)newValue);
				return;
			case BookingPackage.BOOKING__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case BookingPackage.BOOKING__ROOM_EXTRAS:
				((EStructuralFeature.Setting)getRoomExtras()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__BOOKING_ID:
				setBookingId(BOOKING_ID_EDEFAULT);
				return;
			case BookingPackage.BOOKING__CUSTOMER:
				setCustomer((Customer)null);
				return;
			case BookingPackage.BOOKING__BOOKING_STATUS:
				setBookingStatus(BOOKING_STATUS_EDEFAULT);
				return;
			case BookingPackage.BOOKING__EXTRA_COSTS:
				setExtraCosts(EXTRA_COSTS_EDEFAULT);
				return;
			case BookingPackage.BOOKING__ROOM_TYPE_TO_INTEGER:
				getRoomTypeToInteger().clear();
				return;
			case BookingPackage.BOOKING__FROM_DAY:
				setFromDay(FROM_DAY_EDEFAULT);
				return;
			case BookingPackage.BOOKING__TO_DAY:
				setToDay(TO_DAY_EDEFAULT);
				return;
			case BookingPackage.BOOKING__ROOMS:
				getRooms().clear();
				return;
			case BookingPackage.BOOKING__ROOM_EXTRAS:
				getRoomExtras().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__BOOKING_ID:
				return bookingId != BOOKING_ID_EDEFAULT;
			case BookingPackage.BOOKING__CUSTOMER:
				return customer != null;
			case BookingPackage.BOOKING__BOOKING_STATUS:
				return bookingStatus != BOOKING_STATUS_EDEFAULT;
			case BookingPackage.BOOKING__EXTRA_COSTS:
				return extraCosts != EXTRA_COSTS_EDEFAULT;
			case BookingPackage.BOOKING__ROOM_TYPE_TO_INTEGER:
				return roomTypeToInteger != null && !roomTypeToInteger.isEmpty();
			case BookingPackage.BOOKING__FROM_DAY:
				return FROM_DAY_EDEFAULT == null ? fromDay != null : !FROM_DAY_EDEFAULT.equals(fromDay);
			case BookingPackage.BOOKING__TO_DAY:
				return TO_DAY_EDEFAULT == null ? toDay != null : !TO_DAY_EDEFAULT.equals(toDay);
			case BookingPackage.BOOKING__ROOMS:
				return rooms != null && !rooms.isEmpty();
			case BookingPackage.BOOKING__ROOM_EXTRAS:
				return roomExtras != null && !roomExtras.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (bookingId: ");
		result.append(bookingId);
		result.append(", bookingStatus: ");
		result.append(bookingStatus);
		result.append(", extraCosts: ");
		result.append(extraCosts);
		result.append(", fromDay: ");
		result.append(fromDay);
		result.append(", toDay: ");
		result.append(toDay);
		result.append(')');
		return result.toString();
	}

} //BookingImpl
