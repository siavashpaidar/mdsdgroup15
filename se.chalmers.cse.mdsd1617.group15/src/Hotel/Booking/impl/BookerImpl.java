/**
 */
package Hotel.Booking.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Booking.Booker;
import Hotel.Booking.Booking;
import Hotel.Booking.BookingPackage;
import Hotel.Booking.BookingStatus;
import Hotel.Entities.CreditCard;
import Hotel.Room.Room;
import Hotel.Room.RoomStatus;
import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Booking.impl.BookerImpl#getCurrentCheckout <em>Current Checkout</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookerImpl extends MinimalEObjectImpl.Container implements Booker {
	/**
	 * The cached value of the '{@link #getCurrentCheckout() <em>Current Checkout</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentCheckout()
	 * @generated
	 * @ordered
	 */
	protected Booking currentCheckout;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booking getCurrentCheckout() {
		/*if (currentCheckout != null && currentCheckout.eIsProxy()) {
			InternalEObject oldCurrentCheckout = (InternalEObject)currentCheckout;
			currentCheckout = (Booking)eResolveProxy(oldCurrentCheckout);
			if (currentCheckout != oldCurrentCheckout) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKER__CURRENT_CHECKOUT, oldCurrentCheckout, currentCheckout));
			}
		}*/
		return currentCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booking basicGetCurrentCheckout() {
		return currentCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentCheckout(Booking newCurrentCheckout) {
		Booking oldCurrentCheckout = currentCheckout;
		currentCheckout = newCurrentCheckout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKER__CURRENT_CHECKOUT, oldCurrentCheckout, currentCheckout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void listBookings() {
		EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
		for(Booking booking : bookings){
			System.out.println(booking.toString());
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void editBooking(int bookingId, Map.Entry<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		if(booking != null){
			booking.setFromDay(fromDay);
			booking.setToDay(toDay);
			booking.setRoomTypeToInteger(roomTypeToInteger);
		} else {
			System.out.println("A booking with ID " + bookingId + " does not exist!");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void cancelBooking(int bookingId) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		if(booking != null){
			booking.setBookingStatus(BookingStatus.CANCELLED);
			for(Room room : booking.getRooms()){
				room.setRoomStatus(RoomStatus.FREE);
			}
		} else {
			System.out.println("A booking with ID " + bookingId + " does not exist!");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkInBooking(int bookingId) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		if(booking != null){
			booking.setBookingStatus(BookingStatus.CHECKED_IN);
		} else {
			System.out.println("A booking with ID " + bookingId + " does not exist!");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutBooking(int bookingId) {
		if(currentCheckout == null) {
			Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
			if(booking != null){
				booking.setBookingStatus(BookingStatus.UNPAID);
				currentCheckout = booking;
				return true;
			} else {
				System.out.println("A booking with ID " + bookingId + " does not exist!");
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * TODO Fix date checks and return
	 */
	public EList<Booking> listCheckIns(Date fromDay, Date toDay) {
		EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
		bookings.stream()
		.filter(b -> b.getBookingStatus() == BookingStatus.CHECKED_IN)
		.forEach(System.out::println);
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * TODO Fix date checks and return
	 */
	public EList<Booking> listCheckOuts(Date fromDay, Date toDay) {
		EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
		bookings.stream()
		.filter(b -> b.getBookingStatus() == BookingStatus.CHECKED_OUT)
		.forEach(System.out::println);
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * 
	 * Why doesnt the customer have a f/l name? :O
	 * THIS METHOD IS NOT FINISHED!
	 * 
	 */
	public int makeBooking(String firstName, String lastName, Map.Entry<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
		
		Booking booking = BookingFactoryImpl.eINSTANCE.createBooking();
		booking.setBookingId(Integer.valueOf(UUID.randomUUID().toString()));
		booking.setBookingStatus(BookingStatus.UNPAID);
		booking.setCustomer(null);
		booking.setFromDay(fromDay);
		booking.setToDay(toDay);
		booking.setRoomTypeToInteger(roomTypeToInteger);
		
		bookings.forEach(b -> {
			b.getRooms().forEach(r -> {
				if (r.getRoomStatus() == RoomStatus.FREE) {
					r.setRoomStatus(RoomStatus.UNPAID);
					b.setBookingStatus(BookingStatus.CONFIRMED);
					bookings.add(b);
				}
			});
		});
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double checkOut(int roomId, int bookingId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean payRoom(int roomId, CreditCard creditCard) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean payBooking(int bookingId, CreditCard creditCard) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKER__CURRENT_CHECKOUT:
				if (resolve) return getCurrentCheckout();
				return basicGetCurrentCheckout();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKER__CURRENT_CHECKOUT:
				setCurrentCheckout((Booking)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKER__CURRENT_CHECKOUT:
				setCurrentCheckout((Booking)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKER__CURRENT_CHECKOUT:
				return currentCheckout != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKER___LIST_BOOKINGS:
				listBookings();
				return null;
			case BookingPackage.BOOKER___EDIT_BOOKING__INT_EMAP_DATE_DATE:
				editBooking((Integer)arguments.get(0), (Map.Entry<RoomType, Integer>)arguments.get(1), (Date)arguments.get(2), (Date)arguments.get(3));
				return null;
			case BookingPackage.BOOKER___CANCEL_BOOKING__INT:
				cancelBooking((Integer)arguments.get(0));
				return null;
			case BookingPackage.BOOKER___CHECK_IN_BOOKING__INT:
				checkInBooking((Integer)arguments.get(0));
				return null;
			case BookingPackage.BOOKER___CHECK_OUT_BOOKING__INT:
				return checkOutBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKER___LIST_CHECK_INS__DATE_DATE:
				return listCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackage.BOOKER___LIST_CHECK_OUTS__DATE_DATE:
				return listCheckOuts((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackage.BOOKER___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE:
				return makeBooking((String)arguments.get(0), (String)arguments.get(1), (Map.Entry<RoomType, Integer>)arguments.get(2), (Date)arguments.get(3), (Date)arguments.get(4));
			case BookingPackage.BOOKER___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKER___CHECK_OUT__INT_INT:
				return checkOut((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKER___PAY_ROOM__INT_CREDITCARD:
				return payRoom((Integer)arguments.get(0), (CreditCard)arguments.get(1));
			case BookingPackage.BOOKER___PAY_BOOKING__INT_CREDITCARD:
				return payBooking((Integer)arguments.get(0), (CreditCard)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookerImpl
