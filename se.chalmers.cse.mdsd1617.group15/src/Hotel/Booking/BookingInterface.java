/**
 */
package Hotel.Booking;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Booking.BookingInterface#getInstance <em>Instance</em>}</li>
 * </ul>
 *
 * @see Hotel.Booking.BookingPackage#getBookingInterface()
 * @model
 * @generated
 */
public interface BookingInterface extends IBookingInterface {
	/**
	 * Returns the value of the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' reference.
	 * @see #setInstance(IBookingInterface)
	 * @see Hotel.Booking.BookingPackage#getBookingInterface_Instance()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookingInterface getInstance();

	/**
	 * Sets the value of the '{@link Hotel.Booking.BookingInterface#getInstance <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance</em>' reference.
	 * @see #getInstance()
	 * @generated
	 */
	void setInstance(IBookingInterface value);

} // BookingInterface
