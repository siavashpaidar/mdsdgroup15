/**
 */
package Hotel.Booking;

import Hotel.Entities.CreditCard;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Booking.Booker#getCurrentCheckout <em>Current Checkout</em>}</li>
 * </ul>
 *
 * @see Hotel.Booking.BookingPackage#getBooker()
 * @model
 * @generated
 */
public interface Booker extends EObject {
	/**
	 * Returns the value of the '<em><b>Current Checkout</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Checkout</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Checkout</em>' reference.
	 * @see #setCurrentCheckout(Booking)
	 * @see Hotel.Booking.BookingPackage#getBooker_CurrentCheckout()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Booking getCurrentCheckout();

	/**
	 * Sets the value of the '{@link Hotel.Booking.Booker#getCurrentCheckout <em>Current Checkout</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Checkout</em>' reference.
	 * @see #getCurrentCheckout()
	 * @generated
	 */
	void setCurrentCheckout(Booking value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void listBookings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIdRequired="true" bookingIdOrdered="false" roomTypeToIntegerMapType="Hotel.Entities.RoomTypeToIntegerMapEntry<Hotel.RoomType.RoomType, org.eclipse.emf.ecore.EIntegerObject>" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	void editBooking(int bookingId, Map.Entry<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	void cancelBooking(int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	void checkInBooking(int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	boolean checkOutBooking(int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	EList<Booking> listCheckIns(Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	EList<Booking> listCheckOuts(Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false" roomTypeToIntegerMapType="Hotel.Entities.RoomTypeToIntegerMapEntry<Hotel.RoomType.RoomType, org.eclipse.emf.ecore.EIntegerObject>" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	int makeBooking(String firstName, String lastName, Map.Entry<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	int checkInRoom(String roomTypeDescription, int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	double checkOut(int roomId, int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false" creditCardRequired="true" creditCardOrdered="false"
	 * @generated
	 */
	boolean payRoom(int roomId, CreditCard creditCard);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIdRequired="true" bookingIdOrdered="false" creditCardRequired="true" creditCardOrdered="false"
	 * @generated
	 */
	boolean payBooking(int bookingId, CreditCard creditCard);

} // Booker
