/**
 */
package Hotel.Booking;

import java.util.Date;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import Hotel.Entities.Extra;
import Hotel.Room.Room;
import Hotel.RoomType.RoomType;
import Hotel.User.Customer.Customer;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Booking.Booking#getBookingId <em>Booking Id</em>}</li>
 *   <li>{@link Hotel.Booking.Booking#getCustomer <em>Customer</em>}</li>
 *   <li>{@link Hotel.Booking.Booking#getBookingStatus <em>Booking Status</em>}</li>
 *   <li>{@link Hotel.Booking.Booking#getExtraCosts <em>Extra Costs</em>}</li>
 *   <li>{@link Hotel.Booking.Booking#getRoomTypeToInteger <em>Room Type To Integer</em>}</li>
 *   <li>{@link Hotel.Booking.Booking#getFromDay <em>From Day</em>}</li>
 *   <li>{@link Hotel.Booking.Booking#getToDay <em>To Day</em>}</li>
 *   <li>{@link Hotel.Booking.Booking#getRooms <em>Rooms</em>}</li>
 *   <li>{@link Hotel.Booking.Booking#getRoomExtras <em>Room Extras</em>}</li>
 * </ul>
 *
 * @see Hotel.Booking.BookingPackage#getBooking()
 * @model
 * @generated
 */
public interface Booking extends EObject {
	/**
	 * Returns the value of the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Id</em>' attribute.
	 * @see #setBookingId(int)
	 * @see Hotel.Booking.BookingPackage#getBooking_BookingId()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getBookingId();

	/**
	 * Sets the value of the '{@link Hotel.Booking.Booking#getBookingId <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Id</em>' attribute.
	 * @see #getBookingId()
	 * @generated
	 */
	void setBookingId(int value);

	/**
	 * Returns the value of the '<em><b>Customer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Customer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Customer</em>' reference.
	 * @see #setCustomer(Customer)
	 * @see Hotel.Booking.BookingPackage#getBooking_Customer()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Customer getCustomer();

	/**
	 * Sets the value of the '{@link Hotel.Booking.Booking#getCustomer <em>Customer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Customer</em>' reference.
	 * @see #getCustomer()
	 * @generated
	 */
	void setCustomer(Customer value);
	
	/**
	 * 
	 * @param rooms
	 * 
	 * @generated NOT
	 */
	public void setRooms(EList<Room> rooms);
	
	/**
	 * 
	 * @param roomExtras
	 * 
	 * @generated NOT
	 */
	public void setExtras(EMap<Room, EList<Extra>> roomExtras);
	
	/**
	 * 
	 * @param roomTypeToInteger
	 * 
	 * @generated NOT
	 */
	
	public void setRoomTypeToInteger(Entry<RoomType, Integer> roomTypeToInteger);

	/**
	 * Returns the value of the '<em><b>Booking Status</b></em>' attribute.
	 * The literals are from the enumeration {@link Hotel.Booking.BookingStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Status</em>' attribute.
	 * @see Hotel.Booking.BookingStatus
	 * @see #setBookingStatus(BookingStatus)
	 * @see Hotel.Booking.BookingPackage#getBooking_BookingStatus()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookingStatus getBookingStatus();

	/**
	 * Sets the value of the '{@link Hotel.Booking.Booking#getBookingStatus <em>Booking Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Status</em>' attribute.
	 * @see Hotel.Booking.BookingStatus
	 * @see #getBookingStatus()
	 * @generated
	 */
	void setBookingStatus(BookingStatus value);

	/**
	 * Returns the value of the '<em><b>Extra Costs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extra Costs</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extra Costs</em>' attribute.
	 * @see #setExtraCosts(double)
	 * @see Hotel.Booking.BookingPackage#getBooking_ExtraCosts()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getExtraCosts();

	/**
	 * Sets the value of the '{@link Hotel.Booking.Booking#getExtraCosts <em>Extra Costs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extra Costs</em>' attribute.
	 * @see #getExtraCosts()
	 * @generated
	 */
	void setExtraCosts(double value);

	/**
	 * Returns the value of the '<em><b>Room Type To Integer</b></em>' map.
	 * The key is of type {@link Hotel.RoomType.RoomType},
	 * and the value is of type {@link java.lang.Integer},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type To Integer</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type To Integer</em>' map.
	 * @see Hotel.Booking.BookingPackage#getBooking_RoomTypeToInteger()
	 * @model mapType="Hotel.Entities.RoomTypeToIntegerMapEntry<Hotel.RoomType.RoomType, org.eclipse.emf.ecore.EIntegerObject>" ordered="false"
	 * @generated
	 */
	EMap<RoomType, Integer> getRoomTypeToInteger();

	/**
	 * Returns the value of the '<em><b>From Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Day</em>' attribute.
	 * @see #setFromDay(Date)
	 * @see Hotel.Booking.BookingPackage#getBooking_FromDay()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getFromDay();

	/**
	 * Sets the value of the '{@link Hotel.Booking.Booking#getFromDay <em>From Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Day</em>' attribute.
	 * @see #getFromDay()
	 * @generated
	 */
	void setFromDay(Date value);

	/**
	 * Returns the value of the '<em><b>To Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Day</em>' attribute.
	 * @see #setToDay(Date)
	 * @see Hotel.Booking.BookingPackage#getBooking_ToDay()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getToDay();

	/**
	 * Sets the value of the '{@link Hotel.Booking.Booking#getToDay <em>To Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Day</em>' attribute.
	 * @see #getToDay()
	 * @generated
	 */
	void setToDay(Date value);

	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' reference list.
	 * The list contents are of type {@link Hotel.Room.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' reference list.
	 * @see Hotel.Booking.BookingPackage#getBooking_Rooms()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Room> getRooms();

	/**
	 * Returns the value of the '<em><b>Room Extras</b></em>' map.
	 * The key is of type {@link Hotel.Room.Room},
	 * and the value is of type list of {@link Hotel.Entities.Extra},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Extras</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Extras</em>' map.
	 * @see Hotel.Booking.BookingPackage#getBooking_RoomExtras()
	 * @model mapType="Hotel.Entities.RoomToExtraMapEntry<Hotel.Room.Room, Hotel.Entities.Extra>" ordered="false"
	 * @generated
	 */
	EMap<Room, EList<Extra>> getRoomExtras();

} // Booking
