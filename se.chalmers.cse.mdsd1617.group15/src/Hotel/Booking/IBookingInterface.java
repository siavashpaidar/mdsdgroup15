/**
 */
package Hotel.Booking;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IBooking Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Booking.IBookingInterface#getBookings <em>Bookings</em>}</li>
 *   <li>{@link Hotel.Booking.IBookingInterface#getBooker <em>Booker</em>}</li>
 * </ul>
 *
 * @see Hotel.Booking.BookingPackage#getIBookingInterface()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IBookingInterface extends EObject {
	/**
	 * Returns the value of the '<em><b>Bookings</b></em>' reference list.
	 * The list contents are of type {@link Hotel.Booking.Booking}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookings</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookings</em>' reference list.
	 * @see Hotel.Booking.BookingPackage#getIBookingInterface_Bookings()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Booking> getBookings();

	/**
	 * Returns the value of the '<em><b>Booker</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booker</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booker</em>' reference.
	 * @see #setBooker(Booker)
	 * @see Hotel.Booking.BookingPackage#getIBookingInterface_Booker()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Booker getBooker();

	/**
	 * Sets the value of the '{@link Hotel.Booking.IBookingInterface#getBooker <em>Booker</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booker</em>' reference.
	 * @see #getBooker()
	 * @generated
	 */
	void setBooker(Booker value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearComponent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	Booking getBookingFromId(int bookingId);

} // IBookingInterface
