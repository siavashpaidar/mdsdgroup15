/**
 */
package Hotel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHotel Startup Provides Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.HotelPackage#getIHotelStartupProvidesInterface()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IHotelStartupProvidesInterface extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model numRoomsRequired="true" numRoomsOrdered="false"
	 * @generated
	 */
	void startup(int numRooms);

} // IHotelStartupProvidesInterface
