/**
 */
package Hotel.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.HotelPackage;
import Hotel.IHotelStartupProvides;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Entities.Extra;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IHotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IHotelStartupProvidesImpl extends MinimalEObjectImpl.Container implements IHotelStartupProvides {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IHotelStartupProvidesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HotelPackage.Literals.IHOTEL_STARTUP_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		BookingInterfaceImpl.basicGetInstance().clearComponent();
		RoomInterfaceImpl.basicGetInstance().clearComponent();
		RoomTypeInterfaceImpl.basicGetInstance().clearComponent();
		
		RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeHandler().addRoomType("Default Name", 1000, 2, new BasicEList<Extra>(),"Default Description");
		for(int i=0; i<numRooms; i++){
			RoomInterfaceImpl.basicGetInstance().getRoomModifier().addRoom(i, "Default Description");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case HotelPackage.IHOTEL_STARTUP_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //IHotelStartupProvidesImpl
