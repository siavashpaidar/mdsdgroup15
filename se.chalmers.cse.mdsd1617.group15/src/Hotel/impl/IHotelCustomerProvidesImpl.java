/**
 */
package Hotel.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.FreeRoomTypesDTO;
import Hotel.HotelPackage;
import Hotel.IHotelCustomerProvides;
import Hotel.Booking.Booking;
import Hotel.Booking.BookingStatus;
import Hotel.Booking.impl.BookingFactoryImpl;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Entities.Extra;
import Hotel.Room.Room;
import Hotel.Room.RoomStatus;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IHotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IHotelCustomerProvidesImpl extends MinimalEObjectImpl.Container implements IHotelCustomerProvides {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IHotelCustomerProvidesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HotelPackage.Literals.IHOTEL_CUSTOMER_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		EList<FreeRoomTypesDTO> freeRoomTypesDtoList = new BasicEList<FreeRoomTypesDTO>();

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date start = format.parse(startDate);
			Date end = format.parse(endDate);
			EMap<RoomType, EList<Room>> freeRooms = RoomInterfaceImpl.basicGetInstance().getRoomLister().searchFreeRooms(start, end, numBeds);
			for(Map.Entry<RoomType, EList<Room>> entry : freeRooms){
				RoomType roomType = entry.getKey();
				FreeRoomTypesDTO freeRoomTypesDto = new FreeRoomTypesDTOImpl();
				freeRoomTypesDto.setRoomTypeDescription(roomType.getDescription());
				freeRoomTypesDto.setNumBeds(roomType.getNumberOfBeds());
				freeRoomTypesDto.setPricePerNight(roomType.getPrice());
				freeRoomTypesDto.setNumFreeRooms(entry.getValue().size());
				freeRoomTypesDtoList.add(freeRoomTypesDto);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return freeRoomTypesDtoList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date from = null;
		Date to = null;

		try {
			from = format.parse(startDate);
			to = format.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(firstName == null || lastName == null || from == null || to == null || firstName.isEmpty() || lastName.isEmpty() || from.after(to)){
			return -1; //Alternative flow (2.1)
		} else {
			Booking booking = BookingFactoryImpl.init().createBooking();
			EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
			int bookingId = bookings.size()+1;
			booking.setBookingId(bookingId);
			booking.setBookingStatus(BookingStatus.UNPAID); //Should maybe mark as unconfirmed as well
			booking.setFromDay(from);
			booking.setToDay(to);
			bookings.add(booking);
			return bookingId;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID);
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription(roomTypeDescription);
		if(roomType != null && booking != null){
			if (booking.getBookingStatus() != null && booking.getBookingStatus().equals(BookingStatus.UNPAID)) {
				//Get free rooms during the booking time period
				EMap<RoomType, EList<Room>> roomTypeRoomMap = RoomInterfaceImpl.basicGetInstance().getRoomLister().searchFreeRooms(booking.getFromDay(), booking.getToDay(),0);
				EList<Room> rooms = roomTypeRoomMap.get(roomType);	//List of rooms of the provided room type
				if(rooms != null && !rooms.isEmpty()){
					rooms.get(0).setRoomStatus(RoomStatus.BOOKED); //Take the first free room
					booking.getRooms().add(rooms.get(0));
					return true;
				}
			}
		}
		System.out.println("The provided booking or room type does not exist!");
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID);
		if(booking != null){
			for(Room room : booking.getRooms()){
				if(room.getRoomStatus().equals(RoomStatus.BOOKED)){
					booking.setBookingStatus(BookingStatus.CONFIRMED);
					return true;
				}
			}
		}
		System.out.println("No booking with ID " + bookingID + " exists!");
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID);
		
		if(booking != null){
			if(BookingInterfaceImpl.basicGetInstance().getBooker().checkOutBooking(bookingID)) {
				double sum = 0;
				for(Room room : booking.getRooms()) { // For all the rooms in the booking
					room.setRoomStatus(RoomStatus.UNPAID); // Should this be UNPAID? i.e look at iniateBooking()
					sum += initiateRoomCheckout(bookingID, room.getRoomID()); // Add the price of the room to the totalprice
				}
				return sum;
			}
		}
		System.out.println("No booking with ID " + bookingID + " exists!");
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {

		Booking currentCheckout = BookingInterfaceImpl.basicGetInstance().getBooker().getCurrentCheckout();

		double price = 0;
		try {			
			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bank = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();

			if(currentCheckout != null && currentCheckout.getBookingStatus() == BookingStatus.UNPAID) {
				for(Room room : currentCheckout.getRooms()) {
					if (room != null){
						room.setRoomStatus(RoomStatus.UNPAID);
						price += room.getRoomType().getPrice();
						if(currentCheckout.getRoomExtras().get(room) != null){
							for (Extra extra : currentCheckout.getRoomExtras().get(room)){
								price += extra.getPrice();
							}
						}
					}
				}
				if (bank.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, price)){
					currentCheckout.setBookingStatus(BookingStatus.PAID);
					return true;
				}
				currentCheckout.getRooms().clear(); // Should it be cleared?
			}
			
			BookingInterfaceImpl.basicGetInstance().getBooker().setCurrentCheckout(null);
		} catch(SOAPException e) {
			return false;
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		if(booking != null){
			for(Room room : booking.getRooms()){
				if (room.getRoomID() == roomNumber && room.getRoomStatus().equals(RoomStatus.OCCUPIED)){
					room.setRoomStatus(RoomStatus.UNPAID);
					double sum = room.getRoomType().getPrice();
					for (Extra extra : room.getRoomType().getExtras()){
						sum += extra.getPrice();
					}
					return sum;
				}
			}
		}
		System.out.println("Initiate room checkout for room " + roomNumber + " was not successful!");
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {

		try {			
			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bank = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
			
			EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
			for (Booking booking : bookings){
				for(Room room : booking.getRooms()) {
					if (room.getRoomID() == roomNumber && room.getRoomStatus() == RoomStatus.UNPAID){
						double price = room.getRoomType().getPrice();
						for (Extra extra : room.getRoomType().getExtras()){		//@TODO Add extra costs from the booking instead of the room type
							price += extra.getPrice();
						}
						if (bank.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, price)){
							room.setRoomStatus(RoomStatus.FREE);
							return true;
						}
					}
				}
				
				if(BookingInterfaceImpl.basicGetInstance().getBooker().getCurrentCheckout() != null){
					BookingInterfaceImpl.basicGetInstance().getBooker().getCurrentCheckout().getRooms().clear(); // Should it be cleared?
					BookingInterfaceImpl.basicGetInstance().getBooker().setCurrentCheckout(null);
				}
			}
			
		} catch(SOAPException e) {
			return false;
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @TODO Move this functionality to RoomClassImpl
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription(roomTypeDescription);
		if(roomType == null){
			return -1;	//Return -1 if the provided room type does not exist
		}
		return RoomInterfaceImpl.basicGetInstance().getRoomClass().checkInRoom(bookingId, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //IHotelCustomerProvidesImpl
