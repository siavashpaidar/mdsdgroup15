/**
 */
package Hotel.User.Customer;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Hotel.User.Customer.CustomerFactory
 * @model kind="package"
 * @generated
 */
public interface CustomerPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Customer";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Hotel/User/Customer.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Hotel.User.Customer";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CustomerPackage eINSTANCE = Hotel.User.Customer.impl.CustomerPackageImpl.init();

	/**
	 * The meta object id for the '{@link Hotel.User.Customer.impl.CustomerImpl <em>Customer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.User.Customer.impl.CustomerImpl
	 * @see Hotel.User.Customer.impl.CustomerPackageImpl#getCustomer()
	 * @generated
	 */
	int CUSTOMER = 0;

	/**
	 * The number of structural features of the '<em>Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER___CHECK_OUT_BOOKING__INT_CREDITCARD = 0;

	/**
	 * The operation id for the '<em>Make Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE = 1;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER___CHECK_IN_ROOM__INT_ROOMTYPE = 2;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER___CHECK_OUT_ROOM__INT_INT = 3;

	/**
	 * The operation id for the '<em>Pay Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER___PAY_ROOM__INT_INT_CREDITCARD = 4;

	/**
	 * The operation id for the '<em>Search Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER___SEARCH_FREE_ROOMS__DATE_DATE_INT = 5;

	/**
	 * The number of operations of the '<em>Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_OPERATION_COUNT = 6;


	/**
	 * Returns the meta object for class '{@link Hotel.User.Customer.Customer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Customer</em>'.
	 * @see Hotel.User.Customer.Customer
	 * @generated
	 */
	EClass getCustomer();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Customer.Customer#checkOutBooking(int, Hotel.Entities.CreditCard) <em>Check Out Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Booking</em>' operation.
	 * @see Hotel.User.Customer.Customer#checkOutBooking(int, Hotel.Entities.CreditCard)
	 * @generated
	 */
	EOperation getCustomer__CheckOutBooking__int_CreditCard();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Customer.Customer#makeBooking(java.lang.String, java.lang.String, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date) <em>Make Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Make Booking</em>' operation.
	 * @see Hotel.User.Customer.Customer#makeBooking(java.lang.String, java.lang.String, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getCustomer__MakeBooking__String_String_EMap_Date_Date();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Customer.Customer#checkInRoom(int, Hotel.RoomType.RoomType) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see Hotel.User.Customer.Customer#checkInRoom(int, Hotel.RoomType.RoomType)
	 * @generated
	 */
	EOperation getCustomer__CheckInRoom__int_RoomType();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Customer.Customer#checkOutRoom(int, int) <em>Check Out Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Room</em>' operation.
	 * @see Hotel.User.Customer.Customer#checkOutRoom(int, int)
	 * @generated
	 */
	EOperation getCustomer__CheckOutRoom__int_int();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Customer.Customer#payRoom(int, int, Hotel.Entities.CreditCard) <em>Pay Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room</em>' operation.
	 * @see Hotel.User.Customer.Customer#payRoom(int, int, Hotel.Entities.CreditCard)
	 * @generated
	 */
	EOperation getCustomer__PayRoom__int_int_CreditCard();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Customer.Customer#searchFreeRooms(java.util.Date, java.util.Date, int) <em>Search Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Search Free Rooms</em>' operation.
	 * @see Hotel.User.Customer.Customer#searchFreeRooms(java.util.Date, java.util.Date, int)
	 * @generated
	 */
	EOperation getCustomer__SearchFreeRooms__Date_Date_int();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CustomerFactory getCustomerFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Hotel.User.Customer.impl.CustomerImpl <em>Customer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.User.Customer.impl.CustomerImpl
		 * @see Hotel.User.Customer.impl.CustomerPackageImpl#getCustomer()
		 * @generated
		 */
		EClass CUSTOMER = eINSTANCE.getCustomer();

		/**
		 * The meta object literal for the '<em><b>Check Out Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUSTOMER___CHECK_OUT_BOOKING__INT_CREDITCARD = eINSTANCE.getCustomer__CheckOutBooking__int_CreditCard();

		/**
		 * The meta object literal for the '<em><b>Make Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUSTOMER___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE = eINSTANCE.getCustomer__MakeBooking__String_String_EMap_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUSTOMER___CHECK_IN_ROOM__INT_ROOMTYPE = eINSTANCE.getCustomer__CheckInRoom__int_RoomType();

		/**
		 * The meta object literal for the '<em><b>Check Out Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUSTOMER___CHECK_OUT_ROOM__INT_INT = eINSTANCE.getCustomer__CheckOutRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUSTOMER___PAY_ROOM__INT_INT_CREDITCARD = eINSTANCE.getCustomer__PayRoom__int_int_CreditCard();

		/**
		 * The meta object literal for the '<em><b>Search Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUSTOMER___SEARCH_FREE_ROOMS__DATE_DATE_INT = eINSTANCE.getCustomer__SearchFreeRooms__Date_Date_int();

	}

} //CustomerPackage
