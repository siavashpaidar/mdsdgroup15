/**
 */
package Hotel.User.Customer.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Booking.Booker;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Entities.CreditCard;
import Hotel.Room.Room;
import Hotel.Room.RoomLister;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.User.Customer.Customer;
import Hotel.User.Customer.CustomerPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Customer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CustomerImpl extends MinimalEObjectImpl.Container implements Customer {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CustomerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CustomerPackage.Literals.CUSTOMER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean checkOutBooking(int bookingId, CreditCard creditCard) {
		Booker booker = BookingInterfaceImpl.basicGetInstance().getBooker();
		return booker.checkOutBooking(bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int makeBooking(String firstName, String lastName, Map.Entry<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		Booker booker = BookingInterfaceImpl.basicGetInstance().getBooker();
		return booker.makeBooking(firstName, lastName, roomTypeToInteger, fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int checkInRoom(int bookingId, RoomType roomType) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean checkOutRoom(int bookingId, int roomId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void payRoom(int bookingId, int roomId, CreditCard creditCard) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<RoomType, EList<Room>> searchFreeRooms(Date fromDay, Date toDay, int minNumberOfBeds) {
		RoomLister roomLister = RoomInterfaceImpl.basicGetInstance().getRoomLister();
		return roomLister.searchFreeRooms(fromDay, toDay, minNumberOfBeds);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case CustomerPackage.CUSTOMER___CHECK_OUT_BOOKING__INT_CREDITCARD:
				return checkOutBooking((Integer)arguments.get(0), (CreditCard)arguments.get(1));
			case CustomerPackage.CUSTOMER___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE:
				return makeBooking((String)arguments.get(0), (String)arguments.get(1), (Map.Entry<RoomType, Integer>)arguments.get(2), (Date)arguments.get(3), (Date)arguments.get(4));
			case CustomerPackage.CUSTOMER___CHECK_IN_ROOM__INT_ROOMTYPE:
				return checkInRoom((Integer)arguments.get(0), (RoomType)arguments.get(1));
			case CustomerPackage.CUSTOMER___CHECK_OUT_ROOM__INT_INT:
				return checkOutRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case CustomerPackage.CUSTOMER___PAY_ROOM__INT_INT_CREDITCARD:
				payRoom((Integer)arguments.get(0), (Integer)arguments.get(1), (CreditCard)arguments.get(2));
				return null;
			case CustomerPackage.CUSTOMER___SEARCH_FREE_ROOMS__DATE_DATE_INT:
				return searchFreeRooms((Date)arguments.get(0), (Date)arguments.get(1), (Integer)arguments.get(2));
		}
		return super.eInvoke(operationID, arguments);
	}

} //CustomerImpl
