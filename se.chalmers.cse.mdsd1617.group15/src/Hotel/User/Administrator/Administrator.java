/**
 */
package Hotel.User.Administrator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import Hotel.Entities.Extra;
import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Administrator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.User.Administrator.AdministratorPackage#getAdministrator()
 * @model
 * @generated
 */
public interface Administrator extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	void removeRoomType(String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	void changeRoomType(String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	void removeRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	void unblockRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	void blockRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNumberRequired="true" roomNumberOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	void addRoom(int roomNumber, String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomTypeRequired="true" roomTypeOrdered="false" priceRequired="true" priceOrdered="false" nrOfBedsRequired="true" nrOfBedsOrdered="false" extrasMany="true" extrasOrdered="false"
	 * @generated
	 */
	void updateRoomType(RoomType roomType, double price, int nrOfBeds, EList<Extra> extras);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomsRequired="true" roomsOrdered="false"
	 * @generated
	 */
	boolean startUpHotelSystem(int rooms);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomTypeRequired="true" roomTypeOrdered="false" priceRequired="true" priceOrdered="false" nrOfBedsRequired="true" nrOfBedsOrdered="false" extrasMany="true" extrasOrdered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	void addRoomType(String roomType, double price, int nrOfBeds, EList<Extra> extras, String description);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypes();

} // Administrator
