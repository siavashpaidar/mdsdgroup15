/**
 */
package Hotel.User.Administrator.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.HotelFactory;
import Hotel.IHotelStartupProvides;
import Hotel.Entities.Extra;
import Hotel.Room.Room;
import Hotel.Room.RoomStatus;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.RoomTypeHandler;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Administrator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AdministratorImpl extends MinimalEObjectImpl.Container implements Administrator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdministratorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdministratorPackage.Literals.ADMINISTRATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeRoomType(String roomTypeDescription) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void changeRoomType(String roomTypeDescription) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeRoomType(RoomType roomType) {
		RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeHandler().removeRoomType(roomType.getName());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeRoom(int roomID) {
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomID);
		RoomInterfaceImpl.basicGetInstance().getRooms().remove(room);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unblockRoom(int roomID) {
		EList<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
		
		rooms.stream()
			.filter(r -> r.getRoomID() == roomID)
			.forEach(r -> r.setRoomStatus(RoomStatus.FREE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void blockRoom(int roomID) {
		RoomInterfaceImpl.basicGetInstance().getRoomModifier().blockRoom(roomID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addRoom(int roomNumber, String roomTypeDescription) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * 
	 * TODO: ADD SETTER FOR RoomType.setExtras(extras)
	 */
	public void updateRoomType(RoomType roomType, double price, int nrOfBeds, EList<Extra> extras) {
		EList<RoomType> roomtypes = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypes();
		
		roomType.setPrice(price);
		roomType.setNumberOfBeds(nrOfBeds);
		//roomType.setExtras(extras);
		
		roomtypes.stream()
			.filter(r -> r.getName() == roomType.getName())
			.forEach(r -> roomtypes.remove(r));
		
		roomtypes.add(roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean startUpHotelSystem(int rooms) {
		IHotelStartupProvides hotel = HotelFactory.eINSTANCE.createIHotelStartupProvides();
		
		try {
			hotel.startup(rooms);
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addRoomType(String roomType, double price, int nrOfBeds, EList<Extra> extras, String description) {
		RoomTypeHandler roomTypeHandler = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeHandler();
		roomTypeHandler.addRoomType(roomType, price, nrOfBeds, extras, description);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomType> getRoomTypes() {
		return RoomTypeInterfaceImpl.basicGetInstance().getRoomTypes();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AdministratorPackage.ADMINISTRATOR___REMOVE_ROOM_TYPE__STRING:
				removeRoomType((String)arguments.get(0));
				return null;
			case AdministratorPackage.ADMINISTRATOR___CHANGE_ROOM_TYPE__STRING:
				changeRoomType((String)arguments.get(0));
				return null;
			case AdministratorPackage.ADMINISTRATOR___REMOVE_ROOM__INT:
				removeRoom((Integer)arguments.get(0));
				return null;
			case AdministratorPackage.ADMINISTRATOR___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
			case AdministratorPackage.ADMINISTRATOR___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
			case AdministratorPackage.ADMINISTRATOR___ADD_ROOM__INT_STRING:
				addRoom((Integer)arguments.get(0), (String)arguments.get(1));
				return null;
			case AdministratorPackage.ADMINISTRATOR___UPDATE_ROOM_TYPE__ROOMTYPE_DOUBLE_INT_ELIST:
				updateRoomType((RoomType)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (EList<Extra>)arguments.get(3));
				return null;
			case AdministratorPackage.ADMINISTRATOR___START_UP_HOTEL_SYSTEM__INT:
				return startUpHotelSystem((Integer)arguments.get(0));
			case AdministratorPackage.ADMINISTRATOR___ADD_ROOM_TYPE__STRING_DOUBLE_INT_ELIST_STRING:
				addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (EList<Extra>)arguments.get(3), (String)arguments.get(4));
				return null;
			case AdministratorPackage.ADMINISTRATOR___GET_ROOM_TYPES:
				return getRoomTypes();
		}
		return super.eInvoke(operationID, arguments);
	}

} //AdministratorImpl
