/**
 */
package Hotel.User.Administrator.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import Hotel.HotelPackage;
import Hotel.Booking.BookingPackage;
import Hotel.Booking.impl.BookingPackageImpl;
import Hotel.Entities.EntitiesPackage;
import Hotel.Entities.impl.EntitiesPackageImpl;
import Hotel.Room.RoomPackage;
import Hotel.Room.impl.RoomPackageImpl;
import Hotel.RoomType.RoomTypePackage;
import Hotel.RoomType.impl.RoomTypePackageImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Administrator.AdministratorPackage;
import Hotel.User.Customer.CustomerPackage;
import Hotel.User.Customer.impl.CustomerPackageImpl;
import Hotel.User.Receptionist.ReceptionistPackage;
import Hotel.User.Receptionist.impl.ReceptionistPackageImpl;
import Hotel.impl.HotelPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AdministratorPackageImpl extends EPackageImpl implements AdministratorPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass administratorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Hotel.User.Administrator.AdministratorPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AdministratorPackageImpl() {
		super(eNS_URI, AdministratorFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AdministratorPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AdministratorPackage init() {
		if (isInited) return (AdministratorPackage)EPackage.Registry.INSTANCE.getEPackage(AdministratorPackage.eNS_URI);

		// Obtain or create and register package
		AdministratorPackageImpl theAdministratorPackage = (AdministratorPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AdministratorPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AdministratorPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		HotelPackageImpl theHotelPackage = (HotelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HotelPackage.eNS_URI) instanceof HotelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HotelPackage.eNS_URI) : HotelPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		ReceptionistPackageImpl theReceptionistPackage = (ReceptionistPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) instanceof ReceptionistPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) : ReceptionistPackage.eINSTANCE);
		CustomerPackageImpl theCustomerPackage = (CustomerPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CustomerPackage.eNS_URI) instanceof CustomerPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CustomerPackage.eNS_URI) : CustomerPackage.eINSTANCE);
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) : RoomTypePackage.eINSTANCE);
		EntitiesPackageImpl theEntitiesPackage = (EntitiesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EntitiesPackage.eNS_URI) instanceof EntitiesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EntitiesPackage.eNS_URI) : EntitiesPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);

		// Create package meta-data objects
		theAdministratorPackage.createPackageContents();
		theHotelPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theReceptionistPackage.createPackageContents();
		theCustomerPackage.createPackageContents();
		theRoomTypePackage.createPackageContents();
		theEntitiesPackage.createPackageContents();
		theRoomPackage.createPackageContents();

		// Initialize created meta-data
		theAdministratorPackage.initializePackageContents();
		theHotelPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theReceptionistPackage.initializePackageContents();
		theCustomerPackage.initializePackageContents();
		theRoomTypePackage.initializePackageContents();
		theEntitiesPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAdministratorPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AdministratorPackage.eNS_URI, theAdministratorPackage);
		return theAdministratorPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdministrator() {
		return administratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__RemoveRoomType__String() {
		return administratorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__ChangeRoomType__String() {
		return administratorEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__RemoveRoom__int() {
		return administratorEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__UnblockRoom__int() {
		return administratorEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__BlockRoom__int() {
		return administratorEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__AddRoom__int_String() {
		return administratorEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__UpdateRoomType__RoomType_double_int_EList() {
		return administratorEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__StartUpHotelSystem__int() {
		return administratorEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__AddRoomType__String_double_int_EList_String() {
		return administratorEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAdministrator__GetRoomTypes() {
		return administratorEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdministratorFactory getAdministratorFactory() {
		return (AdministratorFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		administratorEClass = createEClass(ADMINISTRATOR);
		createEOperation(administratorEClass, ADMINISTRATOR___REMOVE_ROOM_TYPE__STRING);
		createEOperation(administratorEClass, ADMINISTRATOR___CHANGE_ROOM_TYPE__STRING);
		createEOperation(administratorEClass, ADMINISTRATOR___REMOVE_ROOM__INT);
		createEOperation(administratorEClass, ADMINISTRATOR___UNBLOCK_ROOM__INT);
		createEOperation(administratorEClass, ADMINISTRATOR___BLOCK_ROOM__INT);
		createEOperation(administratorEClass, ADMINISTRATOR___ADD_ROOM__INT_STRING);
		createEOperation(administratorEClass, ADMINISTRATOR___UPDATE_ROOM_TYPE__ROOMTYPE_DOUBLE_INT_ELIST);
		createEOperation(administratorEClass, ADMINISTRATOR___START_UP_HOTEL_SYSTEM__INT);
		createEOperation(administratorEClass, ADMINISTRATOR___ADD_ROOM_TYPE__STRING_DOUBLE_INT_ELIST_STRING);
		createEOperation(administratorEClass, ADMINISTRATOR___GET_ROOM_TYPES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RoomTypePackage theRoomTypePackage = (RoomTypePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI);
		EntitiesPackage theEntitiesPackage = (EntitiesPackage)EPackage.Registry.INSTANCE.getEPackage(EntitiesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(administratorEClass, Administrator.class, "Administrator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getAdministrator__RemoveRoomType__String(), null, "removeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getAdministrator__ChangeRoomType__String(), null, "changeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getAdministrator__RemoveRoom__int(), null, "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getAdministrator__UnblockRoom__int(), null, "unblockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getAdministrator__BlockRoom__int(), null, "blockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getAdministrator__AddRoom__int_String(), null, "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getAdministrator__UpdateRoomType__RoomType_double_int_EList(), null, "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomTypePackage.getRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "nrOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEntitiesPackage.getExtra(), "extras", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getAdministrator__StartUpHotelSystem__int(), ecorePackage.getEBoolean(), "startUpHotelSystem", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "rooms", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getAdministrator__AddRoomType__String_double_int_EList_String(), null, "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "nrOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEntitiesPackage.getExtra(), "extras", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getAdministrator__GetRoomTypes(), theRoomTypePackage.getRoomType(), "getRoomTypes", 0, -1, IS_UNIQUE, !IS_ORDERED);
	}

} //AdministratorPackageImpl
