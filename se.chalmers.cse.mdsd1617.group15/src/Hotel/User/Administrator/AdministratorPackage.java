/**
 */
package Hotel.User.Administrator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Hotel.User.Administrator.AdministratorFactory
 * @model kind="package"
 * @generated
 */
public interface AdministratorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Administrator";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Hotel/User/Administrator.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Hotel.User.Administrator";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AdministratorPackage eINSTANCE = Hotel.User.Administrator.impl.AdministratorPackageImpl.init();

	/**
	 * The meta object id for the '{@link Hotel.User.Administrator.impl.AdministratorImpl <em>Administrator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.User.Administrator.impl.AdministratorImpl
	 * @see Hotel.User.Administrator.impl.AdministratorPackageImpl#getAdministrator()
	 * @generated
	 */
	int ADMINISTRATOR = 0;

	/**
	 * The number of structural features of the '<em>Administrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___REMOVE_ROOM_TYPE__STRING = 0;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___CHANGE_ROOM_TYPE__STRING = 1;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___REMOVE_ROOM__INT = 2;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___UNBLOCK_ROOM__INT = 3;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___BLOCK_ROOM__INT = 4;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___ADD_ROOM__INT_STRING = 5;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___UPDATE_ROOM_TYPE__ROOMTYPE_DOUBLE_INT_ELIST = 6;

	/**
	 * The operation id for the '<em>Start Up Hotel System</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___START_UP_HOTEL_SYSTEM__INT = 7;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___ADD_ROOM_TYPE__STRING_DOUBLE_INT_ELIST_STRING = 8;

	/**
	 * The operation id for the '<em>Get Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR___GET_ROOM_TYPES = 9;

	/**
	 * The number of operations of the '<em>Administrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATOR_OPERATION_COUNT = 10;


	/**
	 * Returns the meta object for class '{@link Hotel.User.Administrator.Administrator <em>Administrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Administrator</em>'.
	 * @see Hotel.User.Administrator.Administrator
	 * @generated
	 */
	EClass getAdministrator();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getAdministrator__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#changeRoomType(java.lang.String) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#changeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getAdministrator__ChangeRoomType__String();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#removeRoom(int)
	 * @generated
	 */
	EOperation getAdministrator__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#unblockRoom(int)
	 * @generated
	 */
	EOperation getAdministrator__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#blockRoom(int)
	 * @generated
	 */
	EOperation getAdministrator__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getAdministrator__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#updateRoomType(Hotel.RoomType.RoomType, double, int, org.eclipse.emf.common.util.EList) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#updateRoomType(Hotel.RoomType.RoomType, double, int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getAdministrator__UpdateRoomType__RoomType_double_int_EList();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#startUpHotelSystem(int) <em>Start Up Hotel System</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Start Up Hotel System</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#startUpHotelSystem(int)
	 * @generated
	 */
	EOperation getAdministrator__StartUpHotelSystem__int();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#addRoomType(java.lang.String, double, int, org.eclipse.emf.common.util.EList, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#addRoomType(java.lang.String, double, int, org.eclipse.emf.common.util.EList, java.lang.String)
	 * @generated
	 */
	EOperation getAdministrator__AddRoomType__String_double_int_EList_String();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Administrator.Administrator#getRoomTypes() <em>Get Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Types</em>' operation.
	 * @see Hotel.User.Administrator.Administrator#getRoomTypes()
	 * @generated
	 */
	EOperation getAdministrator__GetRoomTypes();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AdministratorFactory getAdministratorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Hotel.User.Administrator.impl.AdministratorImpl <em>Administrator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.User.Administrator.impl.AdministratorImpl
		 * @see Hotel.User.Administrator.impl.AdministratorPackageImpl#getAdministrator()
		 * @generated
		 */
		EClass ADMINISTRATOR = eINSTANCE.getAdministrator();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getAdministrator__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___CHANGE_ROOM_TYPE__STRING = eINSTANCE.getAdministrator__ChangeRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___REMOVE_ROOM__INT = eINSTANCE.getAdministrator__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___UNBLOCK_ROOM__INT = eINSTANCE.getAdministrator__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___BLOCK_ROOM__INT = eINSTANCE.getAdministrator__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___ADD_ROOM__INT_STRING = eINSTANCE.getAdministrator__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___UPDATE_ROOM_TYPE__ROOMTYPE_DOUBLE_INT_ELIST = eINSTANCE.getAdministrator__UpdateRoomType__RoomType_double_int_EList();

		/**
		 * The meta object literal for the '<em><b>Start Up Hotel System</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___START_UP_HOTEL_SYSTEM__INT = eINSTANCE.getAdministrator__StartUpHotelSystem__int();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___ADD_ROOM_TYPE__STRING_DOUBLE_INT_ELIST_STRING = eINSTANCE.getAdministrator__AddRoomType__String_double_int_EList_String();

		/**
		 * The meta object literal for the '<em><b>Get Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMINISTRATOR___GET_ROOM_TYPES = eINSTANCE.getAdministrator__GetRoomTypes();

	}

} //AdministratorPackage
