/**
 */
package Hotel.User.Receptionist;

import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import Hotel.Booking.Booking;
import Hotel.Entities.CreditCard;
import Hotel.Entities.Extra;
import Hotel.Room.Room;
import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receptionist</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.User.Receptionist.ReceptionistPackage#getReceptionist()
 * @model
 * @generated
 */
public interface Receptionist extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	EList<Booking> listCheckOuts(Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false"
	 * @generated
	 */
	EList<Booking> listBookings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	EList<Booking> listCheckIns(Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	void cancelBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false" roomMany="true" roomOrdered="false"
	 * @generated
	 */
	void checkInBooking(int bookingID, EList<Room> room);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" creditCardRequired="true" creditCardOrdered="false"
	 * @generated
	 */
	boolean checkOutBooking(int bookingID, CreditCard creditCard);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="Hotel.Entities.RoomTypeToRoomMapEntry<Hotel.RoomType.RoomType, Hotel.Room.Room>" ordered="false" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false" minNumberOfBedsRequired="true" minNumberOfBedsOrdered="false"
	 * @generated
	 */
	EMap<RoomType, EList<Room>> searchFreeRooms(Date fromDay, Date toDay, int minNumberOfBeds);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dayRequired="true" dayOrdered="false"
	 * @generated
	 */
	EList<Room> listOccupiedRooms(Date day);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false" roomIDRequired="true" roomIDOrdered="false" creditCardRequired="true" creditCardOrdered="false"
	 * @generated
	 */
	void payRoom(int bookingID, int roomID, CreditCard creditCard);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	int checkInRoom(int bookingID, RoomType roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean checkOutRoom(int bookingID, int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false" roomIDRequired="true" roomIDOrdered="false" extraRequired="true" extraOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	void addRoomExtra(int bookingID, int roomID, Extra extra, double price);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false" roomTypeToIntegerMapType="Hotel.Entities.RoomTypeToIntegerMapEntry<Hotel.RoomType.RoomType, org.eclipse.emf.ecore.EIntegerObject>" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	void editBooking(int bookingID, Map.Entry<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false" roomTypeToIntegerMapType="Hotel.Entities.RoomTypeToIntegerMapEntry<Hotel.RoomType.RoomType, org.eclipse.emf.ecore.EIntegerObject>" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	int makeBooking(String firstName, String lastName, Map.Entry<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay);

} // Receptionist
