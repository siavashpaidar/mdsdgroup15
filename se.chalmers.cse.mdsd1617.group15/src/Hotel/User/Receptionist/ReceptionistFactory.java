/**
 */
package Hotel.User.Receptionist;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Hotel.User.Receptionist.ReceptionistPackage
 * @generated
 */
public interface ReceptionistFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ReceptionistFactory eINSTANCE = Hotel.User.Receptionist.impl.ReceptionistFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Receptionist</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Receptionist</em>'.
	 * @generated
	 */
	Receptionist createReceptionist();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ReceptionistPackage getReceptionistPackage();

} //ReceptionistFactory
