/**
 */
package Hotel.User.Receptionist.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import Hotel.HotelPackage;
import Hotel.Booking.BookingPackage;
import Hotel.Booking.impl.BookingPackageImpl;
import Hotel.Entities.EntitiesPackage;
import Hotel.Entities.impl.EntitiesPackageImpl;
import Hotel.Room.RoomPackage;
import Hotel.Room.impl.RoomPackageImpl;
import Hotel.RoomType.RoomTypePackage;
import Hotel.RoomType.impl.RoomTypePackageImpl;
import Hotel.User.Administrator.AdministratorPackage;
import Hotel.User.Administrator.impl.AdministratorPackageImpl;
import Hotel.User.Customer.CustomerPackage;
import Hotel.User.Customer.impl.CustomerPackageImpl;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;
import Hotel.User.Receptionist.ReceptionistPackage;
import Hotel.impl.HotelPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ReceptionistPackageImpl extends EPackageImpl implements ReceptionistPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass receptionistEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Hotel.User.Receptionist.ReceptionistPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ReceptionistPackageImpl() {
		super(eNS_URI, ReceptionistFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ReceptionistPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ReceptionistPackage init() {
		if (isInited) return (ReceptionistPackage)EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI);

		// Obtain or create and register package
		ReceptionistPackageImpl theReceptionistPackage = (ReceptionistPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ReceptionistPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ReceptionistPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		HotelPackageImpl theHotelPackage = (HotelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HotelPackage.eNS_URI) instanceof HotelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HotelPackage.eNS_URI) : HotelPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		AdministratorPackageImpl theAdministratorPackage = (AdministratorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdministratorPackage.eNS_URI) instanceof AdministratorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdministratorPackage.eNS_URI) : AdministratorPackage.eINSTANCE);
		CustomerPackageImpl theCustomerPackage = (CustomerPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CustomerPackage.eNS_URI) instanceof CustomerPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CustomerPackage.eNS_URI) : CustomerPackage.eINSTANCE);
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) : RoomTypePackage.eINSTANCE);
		EntitiesPackageImpl theEntitiesPackage = (EntitiesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EntitiesPackage.eNS_URI) instanceof EntitiesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EntitiesPackage.eNS_URI) : EntitiesPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);

		// Create package meta-data objects
		theReceptionistPackage.createPackageContents();
		theHotelPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theAdministratorPackage.createPackageContents();
		theCustomerPackage.createPackageContents();
		theRoomTypePackage.createPackageContents();
		theEntitiesPackage.createPackageContents();
		theRoomPackage.createPackageContents();

		// Initialize created meta-data
		theReceptionistPackage.initializePackageContents();
		theHotelPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theAdministratorPackage.initializePackageContents();
		theCustomerPackage.initializePackageContents();
		theRoomTypePackage.initializePackageContents();
		theEntitiesPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theReceptionistPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ReceptionistPackage.eNS_URI, theReceptionistPackage);
		return theReceptionistPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReceptionist() {
		return receptionistEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__ListCheckOuts__Date_Date() {
		return receptionistEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__ListBookings() {
		return receptionistEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__ListCheckIns__Date_Date() {
		return receptionistEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__CancelBooking__int() {
		return receptionistEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__CheckInBooking__int_EList() {
		return receptionistEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__CheckOutBooking__int_CreditCard() {
		return receptionistEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__SearchFreeRooms__Date_Date_int() {
		return receptionistEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__ListOccupiedRooms__Date() {
		return receptionistEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__PayRoom__int_int_CreditCard() {
		return receptionistEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__CheckInRoom__int_RoomType() {
		return receptionistEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__CheckOutRoom__int_int() {
		return receptionistEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__AddRoomExtra__int_int_Extra_double() {
		return receptionistEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__EditBooking__int_EMap_Date_Date() {
		return receptionistEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceptionist__MakeBooking__String_String_EMap_Date_Date() {
		return receptionistEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReceptionistFactory getReceptionistFactory() {
		return (ReceptionistFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		receptionistEClass = createEClass(RECEPTIONIST);
		createEOperation(receptionistEClass, RECEPTIONIST___LIST_CHECK_OUTS__DATE_DATE);
		createEOperation(receptionistEClass, RECEPTIONIST___LIST_BOOKINGS);
		createEOperation(receptionistEClass, RECEPTIONIST___LIST_CHECK_INS__DATE_DATE);
		createEOperation(receptionistEClass, RECEPTIONIST___CANCEL_BOOKING__INT);
		createEOperation(receptionistEClass, RECEPTIONIST___CHECK_IN_BOOKING__INT_ELIST);
		createEOperation(receptionistEClass, RECEPTIONIST___CHECK_OUT_BOOKING__INT_CREDITCARD);
		createEOperation(receptionistEClass, RECEPTIONIST___SEARCH_FREE_ROOMS__DATE_DATE_INT);
		createEOperation(receptionistEClass, RECEPTIONIST___LIST_OCCUPIED_ROOMS__DATE);
		createEOperation(receptionistEClass, RECEPTIONIST___PAY_ROOM__INT_INT_CREDITCARD);
		createEOperation(receptionistEClass, RECEPTIONIST___CHECK_IN_ROOM__INT_ROOMTYPE);
		createEOperation(receptionistEClass, RECEPTIONIST___CHECK_OUT_ROOM__INT_INT);
		createEOperation(receptionistEClass, RECEPTIONIST___ADD_ROOM_EXTRA__INT_INT_EXTRA_DOUBLE);
		createEOperation(receptionistEClass, RECEPTIONIST___EDIT_BOOKING__INT_EMAP_DATE_DATE);
		createEOperation(receptionistEClass, RECEPTIONIST___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BookingPackage theBookingPackage = (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);
		RoomPackage theRoomPackage = (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);
		EntitiesPackage theEntitiesPackage = (EntitiesPackage)EPackage.Registry.INSTANCE.getEPackage(EntitiesPackage.eNS_URI);
		RoomTypePackage theRoomTypePackage = (RoomTypePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(receptionistEClass, Receptionist.class, "Receptionist", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getReceptionist__ListCheckOuts__Date_Date(), theBookingPackage.getBooking(), "listCheckOuts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "fromDay", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "toDay", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getReceptionist__ListBookings(), theBookingPackage.getBooking(), "listBookings", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__ListCheckIns__Date_Date(), theBookingPackage.getBooking(), "listCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "fromDay", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "toDay", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__CancelBooking__int(), null, "cancelBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__CheckInBooking__int_EList(), null, "checkInBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomPackage.getRoom(), "room", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__CheckOutBooking__int_CreditCard(), ecorePackage.getEBoolean(), "checkOutBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEntitiesPackage.getCreditCard(), "creditCard", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__SearchFreeRooms__Date_Date_int(), theEntitiesPackage.getRoomTypeToRoomMapEntry(), "searchFreeRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "fromDay", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "toDay", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "minNumberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__ListOccupiedRooms__Date(), theRoomPackage.getRoom(), "listOccupiedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "day", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__PayRoom__int_int_CreditCard(), null, "payRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEntitiesPackage.getCreditCard(), "creditCard", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__CheckInRoom__int_RoomType(), ecorePackage.getEInt(), "checkInRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomTypePackage.getRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__CheckOutRoom__int_int(), ecorePackage.getEBoolean(), "checkOutRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__AddRoomExtra__int_int_Extra_double(), null, "addRoomExtra", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEntitiesPackage.getExtra(), "extra", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__EditBooking__int_EMap_Date_Date(), null, "editBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEntitiesPackage.getRoomTypeToIntegerMapEntry(), "roomTypeToInteger", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "fromDay", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "toDay", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getReceptionist__MakeBooking__String_String_EMap_Date_Date(), ecorePackage.getEInt(), "makeBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEntitiesPackage.getRoomTypeToIntegerMapEntry(), "roomTypeToInteger", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "fromDay", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "toDay", 1, 1, IS_UNIQUE, !IS_ORDERED);
	}

} //ReceptionistPackageImpl
