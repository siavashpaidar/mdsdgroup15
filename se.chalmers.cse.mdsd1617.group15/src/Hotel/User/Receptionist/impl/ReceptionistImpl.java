/**
 */
package Hotel.User.Receptionist.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Entities.CreditCard;
import Hotel.Entities.Extra;
import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receptionist</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReceptionistImpl extends MinimalEObjectImpl.Container implements Receptionist {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReceptionistImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ReceptionistPackage.Literals.RECEPTIONIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckOuts(Date fromDay, Date toDay) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().listCheckOuts(fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listBookings() {
		BookingInterfaceImpl.basicGetInstance().getBooker().listBookings();
		return null;	//TODO Fix return statement
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckIns(Date fromDay, Date toDay) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().listCheckIns(fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void cancelBooking(int bookingID) {
		BookingInterfaceImpl.basicGetInstance().getBooker().cancelBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * TODO Investigate the room parameter
	 */
	public void checkInBooking(int bookingID, EList<Room> room) {
		BookingInterfaceImpl.basicGetInstance().getBooker().cancelBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * TODO Investigate the creditCard parameter
	 */
	public boolean checkOutBooking(int bookingID, CreditCard creditCard) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().checkOutBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<RoomType, EList<Room>> searchFreeRooms(Date fromDay, Date toDay, int minNumberOfBeds) {
		return RoomInterfaceImpl.basicGetInstance().getRoomLister().searchFreeRooms(fromDay, toDay, minNumberOfBeds);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> listOccupiedRooms(Date day) {
		return RoomInterfaceImpl.basicGetInstance().getRoomLister().listOccupiedRooms(day);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void payRoom(int bookingID, int roomID, CreditCard creditCard) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(int bookingID, RoomType roomType) {
		return RoomInterfaceImpl.basicGetInstance().getRoomClass().checkInRoom(bookingID, roomType.getDescription());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean checkOutRoom(int bookingID, int roomID) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addRoomExtra(int bookingID, int roomID, Extra extra, double price) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void editBooking(int bookingID, Map.Entry<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		BookingInterfaceImpl.basicGetInstance().getBooker().editBooking(bookingID, roomTypeToInteger, fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int makeBooking(String firstName, String lastName, Map.Entry<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().makeBooking(firstName, lastName, roomTypeToInteger, fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ReceptionistPackage.RECEPTIONIST___LIST_CHECK_OUTS__DATE_DATE:
				return listCheckOuts((Date)arguments.get(0), (Date)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___LIST_BOOKINGS:
				return listBookings();
			case ReceptionistPackage.RECEPTIONIST___LIST_CHECK_INS__DATE_DATE:
				return listCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___CANCEL_BOOKING__INT:
				cancelBooking((Integer)arguments.get(0));
				return null;
			case ReceptionistPackage.RECEPTIONIST___CHECK_IN_BOOKING__INT_ELIST:
				checkInBooking((Integer)arguments.get(0), (EList<Room>)arguments.get(1));
				return null;
			case ReceptionistPackage.RECEPTIONIST___CHECK_OUT_BOOKING__INT_CREDITCARD:
				return checkOutBooking((Integer)arguments.get(0), (CreditCard)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___SEARCH_FREE_ROOMS__DATE_DATE_INT:
				return searchFreeRooms((Date)arguments.get(0), (Date)arguments.get(1), (Integer)arguments.get(2));
			case ReceptionistPackage.RECEPTIONIST___LIST_OCCUPIED_ROOMS__DATE:
				return listOccupiedRooms((Date)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___PAY_ROOM__INT_INT_CREDITCARD:
				payRoom((Integer)arguments.get(0), (Integer)arguments.get(1), (CreditCard)arguments.get(2));
				return null;
			case ReceptionistPackage.RECEPTIONIST___CHECK_IN_ROOM__INT_ROOMTYPE:
				return checkInRoom((Integer)arguments.get(0), (RoomType)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___CHECK_OUT_ROOM__INT_INT:
				return checkOutRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___ADD_ROOM_EXTRA__INT_INT_EXTRA_DOUBLE:
				addRoomExtra((Integer)arguments.get(0), (Integer)arguments.get(1), (Extra)arguments.get(2), (Double)arguments.get(3));
				return null;
			case ReceptionistPackage.RECEPTIONIST___EDIT_BOOKING__INT_EMAP_DATE_DATE:
				editBooking((Integer)arguments.get(0), (Map.Entry<RoomType, Integer>)arguments.get(1), (Date)arguments.get(2), (Date)arguments.get(3));
				return null;
			case ReceptionistPackage.RECEPTIONIST___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE:
				return makeBooking((String)arguments.get(0), (String)arguments.get(1), (Map.Entry<RoomType, Integer>)arguments.get(2), (Date)arguments.get(3), (Date)arguments.get(4));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ReceptionistImpl
