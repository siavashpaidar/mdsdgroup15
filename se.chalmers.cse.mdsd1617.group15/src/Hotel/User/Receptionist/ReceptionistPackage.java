/**
 */
package Hotel.User.Receptionist;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Hotel.User.Receptionist.ReceptionistFactory
 * @model kind="package"
 * @generated
 */
public interface ReceptionistPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Receptionist";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Hotel/User/Receptionist.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Hotel.User.Receptionist";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ReceptionistPackage eINSTANCE = Hotel.User.Receptionist.impl.ReceptionistPackageImpl.init();

	/**
	 * The meta object id for the '{@link Hotel.User.Receptionist.impl.ReceptionistImpl <em>Receptionist</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.User.Receptionist.impl.ReceptionistImpl
	 * @see Hotel.User.Receptionist.impl.ReceptionistPackageImpl#getReceptionist()
	 * @generated
	 */
	int RECEPTIONIST = 0;

	/**
	 * The number of structural features of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_CHECK_OUTS__DATE_DATE = 0;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_BOOKINGS = 1;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_CHECK_INS__DATE_DATE = 2;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CANCEL_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_IN_BOOKING__INT_ELIST = 4;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_OUT_BOOKING__INT_CREDITCARD = 5;

	/**
	 * The operation id for the '<em>Search Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___SEARCH_FREE_ROOMS__DATE_DATE_INT = 6;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_OCCUPIED_ROOMS__DATE = 7;

	/**
	 * The operation id for the '<em>Pay Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___PAY_ROOM__INT_INT_CREDITCARD = 8;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_IN_ROOM__INT_ROOMTYPE = 9;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_OUT_ROOM__INT_INT = 10;

	/**
	 * The operation id for the '<em>Add Room Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___ADD_ROOM_EXTRA__INT_INT_EXTRA_DOUBLE = 11;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___EDIT_BOOKING__INT_EMAP_DATE_DATE = 12;

	/**
	 * The operation id for the '<em>Make Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE = 13;

	/**
	 * The number of operations of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_OPERATION_COUNT = 14;


	/**
	 * Returns the meta object for class '{@link Hotel.User.Receptionist.Receptionist <em>Receptionist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receptionist</em>'.
	 * @see Hotel.User.Receptionist.Receptionist
	 * @generated
	 */
	EClass getReceptionist();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#listCheckOuts(java.util.Date, java.util.Date) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#listCheckOuts(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getReceptionist__ListCheckOuts__Date_Date();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#listBookings()
	 * @generated
	 */
	EOperation getReceptionist__ListBookings();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#listCheckIns(java.util.Date, java.util.Date) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#listCheckIns(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getReceptionist__ListCheckIns__Date_Date();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#cancelBooking(int)
	 * @generated
	 */
	EOperation getReceptionist__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#checkInBooking(int, org.eclipse.emf.common.util.EList) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#checkInBooking(int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getReceptionist__CheckInBooking__int_EList();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#checkOutBooking(int, Hotel.Entities.CreditCard) <em>Check Out Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Booking</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#checkOutBooking(int, Hotel.Entities.CreditCard)
	 * @generated
	 */
	EOperation getReceptionist__CheckOutBooking__int_CreditCard();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#searchFreeRooms(java.util.Date, java.util.Date, int) <em>Search Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Search Free Rooms</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#searchFreeRooms(java.util.Date, java.util.Date, int)
	 * @generated
	 */
	EOperation getReceptionist__SearchFreeRooms__Date_Date_int();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#listOccupiedRooms(java.util.Date) <em>List Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Occupied Rooms</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#listOccupiedRooms(java.util.Date)
	 * @generated
	 */
	EOperation getReceptionist__ListOccupiedRooms__Date();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#payRoom(int, int, Hotel.Entities.CreditCard) <em>Pay Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#payRoom(int, int, Hotel.Entities.CreditCard)
	 * @generated
	 */
	EOperation getReceptionist__PayRoom__int_int_CreditCard();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#checkInRoom(int, Hotel.RoomType.RoomType) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#checkInRoom(int, Hotel.RoomType.RoomType)
	 * @generated
	 */
	EOperation getReceptionist__CheckInRoom__int_RoomType();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#checkOutRoom(int, int) <em>Check Out Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Room</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#checkOutRoom(int, int)
	 * @generated
	 */
	EOperation getReceptionist__CheckOutRoom__int_int();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#addRoomExtra(int, int, Hotel.Entities.Extra, double) <em>Add Room Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Extra</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#addRoomExtra(int, int, Hotel.Entities.Extra, double)
	 * @generated
	 */
	EOperation getReceptionist__AddRoomExtra__int_int_Extra_double();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#editBooking(int, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#editBooking(int, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getReceptionist__EditBooking__int_EMap_Date_Date();

	/**
	 * Returns the meta object for the '{@link Hotel.User.Receptionist.Receptionist#makeBooking(java.lang.String, java.lang.String, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date) <em>Make Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Make Booking</em>' operation.
	 * @see Hotel.User.Receptionist.Receptionist#makeBooking(java.lang.String, java.lang.String, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getReceptionist__MakeBooking__String_String_EMap_Date_Date();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ReceptionistFactory getReceptionistFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Hotel.User.Receptionist.impl.ReceptionistImpl <em>Receptionist</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.User.Receptionist.impl.ReceptionistImpl
		 * @see Hotel.User.Receptionist.impl.ReceptionistPackageImpl#getReceptionist()
		 * @generated
		 */
		EClass RECEPTIONIST = eINSTANCE.getReceptionist();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_CHECK_OUTS__DATE_DATE = eINSTANCE.getReceptionist__ListCheckOuts__Date_Date();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_BOOKINGS = eINSTANCE.getReceptionist__ListBookings();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_CHECK_INS__DATE_DATE = eINSTANCE.getReceptionist__ListCheckIns__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CANCEL_BOOKING__INT = eINSTANCE.getReceptionist__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CHECK_IN_BOOKING__INT_ELIST = eINSTANCE.getReceptionist__CheckInBooking__int_EList();

		/**
		 * The meta object literal for the '<em><b>Check Out Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CHECK_OUT_BOOKING__INT_CREDITCARD = eINSTANCE.getReceptionist__CheckOutBooking__int_CreditCard();

		/**
		 * The meta object literal for the '<em><b>Search Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___SEARCH_FREE_ROOMS__DATE_DATE_INT = eINSTANCE.getReceptionist__SearchFreeRooms__Date_Date_int();

		/**
		 * The meta object literal for the '<em><b>List Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_OCCUPIED_ROOMS__DATE = eINSTANCE.getReceptionist__ListOccupiedRooms__Date();

		/**
		 * The meta object literal for the '<em><b>Pay Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___PAY_ROOM__INT_INT_CREDITCARD = eINSTANCE.getReceptionist__PayRoom__int_int_CreditCard();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CHECK_IN_ROOM__INT_ROOMTYPE = eINSTANCE.getReceptionist__CheckInRoom__int_RoomType();

		/**
		 * The meta object literal for the '<em><b>Check Out Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CHECK_OUT_ROOM__INT_INT = eINSTANCE.getReceptionist__CheckOutRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Add Room Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___ADD_ROOM_EXTRA__INT_INT_EXTRA_DOUBLE = eINSTANCE.getReceptionist__AddRoomExtra__int_int_Extra_double();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___EDIT_BOOKING__INT_EMAP_DATE_DATE = eINSTANCE.getReceptionist__EditBooking__int_EMap_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Make Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE = eINSTANCE.getReceptionist__MakeBooking__String_String_EMap_Date_Date();

	}

} //ReceptionistPackage
