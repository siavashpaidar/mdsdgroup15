/**
 */
package Hotel.Entities.util;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import Hotel.Booking.Booking;
import Hotel.Entities.CreditCard;
import Hotel.Entities.EntitiesPackage;
import Hotel.Entities.Extra;
import Hotel.Room.Room;
import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see Hotel.Entities.EntitiesPackage
 * @generated
 */
public class EntitiesSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EntitiesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntitiesSwitch() {
		if (modelPackage == null) {
			modelPackage = EntitiesPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case EntitiesPackage.CREDIT_CARD: {
				CreditCard creditCard = (CreditCard)theEObject;
				T result = caseCreditCard(creditCard);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EntitiesPackage.ROOM_TYPE_TO_ROOM_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<RoomType, EList<Room>> roomTypeToRoomMapEntry = (Map.Entry<RoomType, EList<Room>>)theEObject;
				T result = caseRoomTypeToRoomMapEntry(roomTypeToRoomMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EntitiesPackage.BOOKING_TO_ROOM_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Booking, Room> bookingToRoomMapEntry = (Map.Entry<Booking, Room>)theEObject;
				T result = caseBookingToRoomMapEntry(bookingToRoomMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EntitiesPackage.ROOM_TYPE_TO_INTEGER_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<RoomType, Integer> roomTypeToIntegerMapEntry = (Map.Entry<RoomType, Integer>)theEObject;
				T result = caseRoomTypeToIntegerMapEntry(roomTypeToIntegerMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EntitiesPackage.ROOM_TO_EXTRA_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Room, EList<Extra>> roomToExtraMapEntry = (Map.Entry<Room, EList<Extra>>)theEObject;
				T result = caseRoomToExtraMapEntry(roomToExtraMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EntitiesPackage.EXTRA: {
				Extra extra = (Extra)theEObject;
				T result = caseExtra(extra);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Credit Card</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Credit Card</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreditCard(CreditCard object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room Type To Room Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room Type To Room Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomTypeToRoomMapEntry(Map.Entry<RoomType, EList<Room>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Booking To Room Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Booking To Room Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBookingToRoomMapEntry(Map.Entry<Booking, Room> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room Type To Integer Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room Type To Integer Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomTypeToIntegerMapEntry(Map.Entry<RoomType, Integer> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room To Extra Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room To Extra Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomToExtraMapEntry(Map.Entry<Room, EList<Extra>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extra</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extra</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtra(Extra object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //EntitiesSwitch
