/**
 */
package Hotel.Entities;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Hotel.Entities.EntitiesFactory
 * @model kind="package"
 * @generated
 */
public interface EntitiesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Entities";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Hotel/Entities.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Hotel.Entities";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EntitiesPackage eINSTANCE = Hotel.Entities.impl.EntitiesPackageImpl.init();

	/**
	 * The meta object id for the '{@link Hotel.Entities.impl.CreditCardImpl <em>Credit Card</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Entities.impl.CreditCardImpl
	 * @see Hotel.Entities.impl.EntitiesPackageImpl#getCreditCard()
	 * @generated
	 */
	int CREDIT_CARD = 0;

	/**
	 * The feature id for the '<em><b>Cc Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_CARD__CC_NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Ccv</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_CARD__CCV = 1;

	/**
	 * The feature id for the '<em><b>Expiry Month</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_CARD__EXPIRY_MONTH = 2;

	/**
	 * The feature id for the '<em><b>Expiry Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_CARD__EXPIRY_YEAR = 3;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_CARD__FIRST_NAME = 4;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_CARD__LAST_NAME = 5;

	/**
	 * The number of structural features of the '<em>Credit Card</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_CARD_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Credit Card</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_CARD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Hotel.Entities.impl.RoomTypeToRoomMapEntryImpl <em>Room Type To Room Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Entities.impl.RoomTypeToRoomMapEntryImpl
	 * @see Hotel.Entities.impl.EntitiesPackageImpl#getRoomTypeToRoomMapEntry()
	 * @generated
	 */
	int ROOM_TYPE_TO_ROOM_MAP_ENTRY = 1;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_ROOM_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_ROOM_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Room Type To Room Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_ROOM_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room Type To Room Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_ROOM_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Hotel.Entities.impl.BookingToRoomMapEntryImpl <em>Booking To Room Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Entities.impl.BookingToRoomMapEntryImpl
	 * @see Hotel.Entities.impl.EntitiesPackageImpl#getBookingToRoomMapEntry()
	 * @generated
	 */
	int BOOKING_TO_ROOM_MAP_ENTRY = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TO_ROOM_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TO_ROOM_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Booking To Room Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TO_ROOM_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Booking To Room Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TO_ROOM_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Hotel.Entities.impl.RoomTypeToIntegerMapEntryImpl <em>Room Type To Integer Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Entities.impl.RoomTypeToIntegerMapEntryImpl
	 * @see Hotel.Entities.impl.EntitiesPackageImpl#getRoomTypeToIntegerMapEntry()
	 * @generated
	 */
	int ROOM_TYPE_TO_INTEGER_MAP_ENTRY = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_INTEGER_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_INTEGER_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Room Type To Integer Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_INTEGER_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room Type To Integer Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TO_INTEGER_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Hotel.Entities.impl.RoomToExtraMapEntryImpl <em>Room To Extra Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Entities.impl.RoomToExtraMapEntryImpl
	 * @see Hotel.Entities.impl.EntitiesPackageImpl#getRoomToExtraMapEntry()
	 * @generated
	 */
	int ROOM_TO_EXTRA_MAP_ENTRY = 4;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TO_EXTRA_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TO_EXTRA_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Room To Extra Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TO_EXTRA_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room To Extra Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TO_EXTRA_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Hotel.Entities.impl.ExtraImpl <em>Extra</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Entities.impl.ExtraImpl
	 * @see Hotel.Entities.impl.EntitiesPackageImpl#getExtra()
	 * @generated
	 */
	int EXTRA = 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA__PRICE = 1;

	/**
	 * The number of structural features of the '<em>Extra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Extra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link Hotel.Entities.CreditCard <em>Credit Card</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Credit Card</em>'.
	 * @see Hotel.Entities.CreditCard
	 * @generated
	 */
	EClass getCreditCard();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Entities.CreditCard#getCcNumber <em>Cc Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cc Number</em>'.
	 * @see Hotel.Entities.CreditCard#getCcNumber()
	 * @see #getCreditCard()
	 * @generated
	 */
	EAttribute getCreditCard_CcNumber();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Entities.CreditCard#getCcv <em>Ccv</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ccv</em>'.
	 * @see Hotel.Entities.CreditCard#getCcv()
	 * @see #getCreditCard()
	 * @generated
	 */
	EAttribute getCreditCard_Ccv();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Entities.CreditCard#getExpiryMonth <em>Expiry Month</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expiry Month</em>'.
	 * @see Hotel.Entities.CreditCard#getExpiryMonth()
	 * @see #getCreditCard()
	 * @generated
	 */
	EAttribute getCreditCard_ExpiryMonth();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Entities.CreditCard#getExpiryYear <em>Expiry Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expiry Year</em>'.
	 * @see Hotel.Entities.CreditCard#getExpiryYear()
	 * @see #getCreditCard()
	 * @generated
	 */
	EAttribute getCreditCard_ExpiryYear();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Entities.CreditCard#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see Hotel.Entities.CreditCard#getFirstName()
	 * @see #getCreditCard()
	 * @generated
	 */
	EAttribute getCreditCard_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Entities.CreditCard#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see Hotel.Entities.CreditCard#getLastName()
	 * @see #getCreditCard()
	 * @generated
	 */
	EAttribute getCreditCard_LastName();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Room Type To Room Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type To Room Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="Hotel.RoomType.RoomType" keyRequired="true" keyOrdered="false"
	 *        valueType="Hotel.Room.Room" valueMany="true" valueOrdered="false"
	 * @generated
	 */
	EClass getRoomTypeToRoomMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoomTypeToRoomMapEntry()
	 * @generated
	 */
	EReference getRoomTypeToRoomMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoomTypeToRoomMapEntry()
	 * @generated
	 */
	EReference getRoomTypeToRoomMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Booking To Room Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking To Room Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="Hotel.Booking.Booking" keyRequired="true" keyOrdered="false"
	 *        valueType="Hotel.Room.Room" valueRequired="true" valueOrdered="false"
	 * @generated
	 */
	EClass getBookingToRoomMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getBookingToRoomMapEntry()
	 * @generated
	 */
	EReference getBookingToRoomMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getBookingToRoomMapEntry()
	 * @generated
	 */
	EReference getBookingToRoomMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Room Type To Integer Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type To Integer Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="Hotel.RoomType.RoomType" keyRequired="true" keyOrdered="false"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject" valueRequired="true" valueOrdered="false"
	 * @generated
	 */
	EClass getRoomTypeToIntegerMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoomTypeToIntegerMapEntry()
	 * @generated
	 */
	EReference getRoomTypeToIntegerMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoomTypeToIntegerMapEntry()
	 * @generated
	 */
	EAttribute getRoomTypeToIntegerMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Room To Extra Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room To Extra Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="Hotel.Room.Room" keyRequired="true" keyOrdered="false"
	 *        valueType="Hotel.Entities.Extra" valueMany="true" valueOrdered="false"
	 * @generated
	 */
	EClass getRoomToExtraMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoomToExtraMapEntry()
	 * @generated
	 */
	EReference getRoomToExtraMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoomToExtraMapEntry()
	 * @generated
	 */
	EReference getRoomToExtraMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link Hotel.Entities.Extra <em>Extra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extra</em>'.
	 * @see Hotel.Entities.Extra
	 * @generated
	 */
	EClass getExtra();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Entities.Extra#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see Hotel.Entities.Extra#getDescription()
	 * @see #getExtra()
	 * @generated
	 */
	EAttribute getExtra_Description();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Entities.Extra#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see Hotel.Entities.Extra#getPrice()
	 * @see #getExtra()
	 * @generated
	 */
	EAttribute getExtra_Price();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EntitiesFactory getEntitiesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Hotel.Entities.impl.CreditCardImpl <em>Credit Card</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Entities.impl.CreditCardImpl
		 * @see Hotel.Entities.impl.EntitiesPackageImpl#getCreditCard()
		 * @generated
		 */
		EClass CREDIT_CARD = eINSTANCE.getCreditCard();

		/**
		 * The meta object literal for the '<em><b>Cc Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_CARD__CC_NUMBER = eINSTANCE.getCreditCard_CcNumber();

		/**
		 * The meta object literal for the '<em><b>Ccv</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_CARD__CCV = eINSTANCE.getCreditCard_Ccv();

		/**
		 * The meta object literal for the '<em><b>Expiry Month</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_CARD__EXPIRY_MONTH = eINSTANCE.getCreditCard_ExpiryMonth();

		/**
		 * The meta object literal for the '<em><b>Expiry Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_CARD__EXPIRY_YEAR = eINSTANCE.getCreditCard_ExpiryYear();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_CARD__FIRST_NAME = eINSTANCE.getCreditCard_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_CARD__LAST_NAME = eINSTANCE.getCreditCard_LastName();

		/**
		 * The meta object literal for the '{@link Hotel.Entities.impl.RoomTypeToRoomMapEntryImpl <em>Room Type To Room Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Entities.impl.RoomTypeToRoomMapEntryImpl
		 * @see Hotel.Entities.impl.EntitiesPackageImpl#getRoomTypeToRoomMapEntry()
		 * @generated
		 */
		EClass ROOM_TYPE_TO_ROOM_MAP_ENTRY = eINSTANCE.getRoomTypeToRoomMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_TO_ROOM_MAP_ENTRY__KEY = eINSTANCE.getRoomTypeToRoomMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_TO_ROOM_MAP_ENTRY__VALUE = eINSTANCE.getRoomTypeToRoomMapEntry_Value();

		/**
		 * The meta object literal for the '{@link Hotel.Entities.impl.BookingToRoomMapEntryImpl <em>Booking To Room Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Entities.impl.BookingToRoomMapEntryImpl
		 * @see Hotel.Entities.impl.EntitiesPackageImpl#getBookingToRoomMapEntry()
		 * @generated
		 */
		EClass BOOKING_TO_ROOM_MAP_ENTRY = eINSTANCE.getBookingToRoomMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_TO_ROOM_MAP_ENTRY__KEY = eINSTANCE.getBookingToRoomMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_TO_ROOM_MAP_ENTRY__VALUE = eINSTANCE.getBookingToRoomMapEntry_Value();

		/**
		 * The meta object literal for the '{@link Hotel.Entities.impl.RoomTypeToIntegerMapEntryImpl <em>Room Type To Integer Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Entities.impl.RoomTypeToIntegerMapEntryImpl
		 * @see Hotel.Entities.impl.EntitiesPackageImpl#getRoomTypeToIntegerMapEntry()
		 * @generated
		 */
		EClass ROOM_TYPE_TO_INTEGER_MAP_ENTRY = eINSTANCE.getRoomTypeToIntegerMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_TO_INTEGER_MAP_ENTRY__KEY = eINSTANCE.getRoomTypeToIntegerMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE_TO_INTEGER_MAP_ENTRY__VALUE = eINSTANCE.getRoomTypeToIntegerMapEntry_Value();

		/**
		 * The meta object literal for the '{@link Hotel.Entities.impl.RoomToExtraMapEntryImpl <em>Room To Extra Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Entities.impl.RoomToExtraMapEntryImpl
		 * @see Hotel.Entities.impl.EntitiesPackageImpl#getRoomToExtraMapEntry()
		 * @generated
		 */
		EClass ROOM_TO_EXTRA_MAP_ENTRY = eINSTANCE.getRoomToExtraMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TO_EXTRA_MAP_ENTRY__KEY = eINSTANCE.getRoomToExtraMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TO_EXTRA_MAP_ENTRY__VALUE = eINSTANCE.getRoomToExtraMapEntry_Value();

		/**
		 * The meta object literal for the '{@link Hotel.Entities.impl.ExtraImpl <em>Extra</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Entities.impl.ExtraImpl
		 * @see Hotel.Entities.impl.EntitiesPackageImpl#getExtra()
		 * @generated
		 */
		EClass EXTRA = eINSTANCE.getExtra();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA__DESCRIPTION = eINSTANCE.getExtra_Description();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA__PRICE = eINSTANCE.getExtra_Price();

	}

} //EntitiesPackage
