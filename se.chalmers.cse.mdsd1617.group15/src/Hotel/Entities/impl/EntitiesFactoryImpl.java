/**
 */
package Hotel.Entities.impl;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import Hotel.Booking.Booking;
import Hotel.Entities.CreditCard;
import Hotel.Entities.EntitiesFactory;
import Hotel.Entities.EntitiesPackage;
import Hotel.Entities.Extra;
import Hotel.Room.Room;
import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EntitiesFactoryImpl extends EFactoryImpl implements EntitiesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EntitiesFactory init() {
		try {
			EntitiesFactory theEntitiesFactory = (EntitiesFactory)EPackage.Registry.INSTANCE.getEFactory(EntitiesPackage.eNS_URI);
			if (theEntitiesFactory != null) {
				return theEntitiesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EntitiesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntitiesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case EntitiesPackage.CREDIT_CARD: return createCreditCard();
			case EntitiesPackage.ROOM_TYPE_TO_ROOM_MAP_ENTRY: return (EObject)createRoomTypeToRoomMapEntry();
			case EntitiesPackage.BOOKING_TO_ROOM_MAP_ENTRY: return (EObject)createBookingToRoomMapEntry();
			case EntitiesPackage.ROOM_TYPE_TO_INTEGER_MAP_ENTRY: return (EObject)createRoomTypeToIntegerMapEntry();
			case EntitiesPackage.ROOM_TO_EXTRA_MAP_ENTRY: return (EObject)createRoomToExtraMapEntry();
			case EntitiesPackage.EXTRA: return createExtra();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreditCard createCreditCard() {
		CreditCardImpl creditCard = new CreditCardImpl();
		return creditCard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<RoomType, EList<Room>> createRoomTypeToRoomMapEntry() {
		RoomTypeToRoomMapEntryImpl roomTypeToRoomMapEntry = new RoomTypeToRoomMapEntryImpl();
		return roomTypeToRoomMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Booking, Room> createBookingToRoomMapEntry() {
		BookingToRoomMapEntryImpl bookingToRoomMapEntry = new BookingToRoomMapEntryImpl();
		return bookingToRoomMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<RoomType, Integer> createRoomTypeToIntegerMapEntry() {
		RoomTypeToIntegerMapEntryImpl roomTypeToIntegerMapEntry = new RoomTypeToIntegerMapEntryImpl();
		return roomTypeToIntegerMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Room, EList<Extra>> createRoomToExtraMapEntry() {
		RoomToExtraMapEntryImpl roomToExtraMapEntry = new RoomToExtraMapEntryImpl();
		return roomToExtraMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Extra createExtra() {
		ExtraImpl extra = new ExtraImpl();
		return extra;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntitiesPackage getEntitiesPackage() {
		return (EntitiesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EntitiesPackage getPackage() {
		return EntitiesPackage.eINSTANCE;
	}

} //EntitiesFactoryImpl
