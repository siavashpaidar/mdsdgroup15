/**
 */
package Hotel.Entities.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import Hotel.HotelPackage;
import Hotel.Booking.BookingPackage;
import Hotel.Booking.impl.BookingPackageImpl;
import Hotel.Entities.CreditCard;
import Hotel.Entities.EntitiesFactory;
import Hotel.Entities.EntitiesPackage;
import Hotel.Entities.Extra;
import Hotel.Room.RoomPackage;
import Hotel.Room.impl.RoomPackageImpl;
import Hotel.RoomType.RoomTypePackage;
import Hotel.RoomType.impl.RoomTypePackageImpl;
import Hotel.User.Administrator.AdministratorPackage;
import Hotel.User.Administrator.impl.AdministratorPackageImpl;
import Hotel.User.Customer.CustomerPackage;
import Hotel.User.Customer.impl.CustomerPackageImpl;
import Hotel.User.Receptionist.ReceptionistPackage;
import Hotel.User.Receptionist.impl.ReceptionistPackageImpl;
import Hotel.impl.HotelPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EntitiesPackageImpl extends EPackageImpl implements EntitiesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass creditCardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeToRoomMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingToRoomMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeToIntegerMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomToExtraMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extraEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Hotel.Entities.EntitiesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EntitiesPackageImpl() {
		super(eNS_URI, EntitiesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link EntitiesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EntitiesPackage init() {
		if (isInited) return (EntitiesPackage)EPackage.Registry.INSTANCE.getEPackage(EntitiesPackage.eNS_URI);

		// Obtain or create and register package
		EntitiesPackageImpl theEntitiesPackage = (EntitiesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof EntitiesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new EntitiesPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		HotelPackageImpl theHotelPackage = (HotelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HotelPackage.eNS_URI) instanceof HotelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HotelPackage.eNS_URI) : HotelPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		AdministratorPackageImpl theAdministratorPackage = (AdministratorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdministratorPackage.eNS_URI) instanceof AdministratorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdministratorPackage.eNS_URI) : AdministratorPackage.eINSTANCE);
		ReceptionistPackageImpl theReceptionistPackage = (ReceptionistPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) instanceof ReceptionistPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) : ReceptionistPackage.eINSTANCE);
		CustomerPackageImpl theCustomerPackage = (CustomerPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CustomerPackage.eNS_URI) instanceof CustomerPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CustomerPackage.eNS_URI) : CustomerPackage.eINSTANCE);
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) : RoomTypePackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);

		// Create package meta-data objects
		theEntitiesPackage.createPackageContents();
		theHotelPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theAdministratorPackage.createPackageContents();
		theReceptionistPackage.createPackageContents();
		theCustomerPackage.createPackageContents();
		theRoomTypePackage.createPackageContents();
		theRoomPackage.createPackageContents();

		// Initialize created meta-data
		theEntitiesPackage.initializePackageContents();
		theHotelPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theAdministratorPackage.initializePackageContents();
		theReceptionistPackage.initializePackageContents();
		theCustomerPackage.initializePackageContents();
		theRoomTypePackage.initializePackageContents();
		theRoomPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEntitiesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EntitiesPackage.eNS_URI, theEntitiesPackage);
		return theEntitiesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreditCard() {
		return creditCardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreditCard_CcNumber() {
		return (EAttribute)creditCardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreditCard_Ccv() {
		return (EAttribute)creditCardEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreditCard_ExpiryMonth() {
		return (EAttribute)creditCardEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreditCard_ExpiryYear() {
		return (EAttribute)creditCardEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreditCard_FirstName() {
		return (EAttribute)creditCardEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreditCard_LastName() {
		return (EAttribute)creditCardEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomTypeToRoomMapEntry() {
		return roomTypeToRoomMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomTypeToRoomMapEntry_Key() {
		return (EReference)roomTypeToRoomMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomTypeToRoomMapEntry_Value() {
		return (EReference)roomTypeToRoomMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookingToRoomMapEntry() {
		return bookingToRoomMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingToRoomMapEntry_Key() {
		return (EReference)bookingToRoomMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingToRoomMapEntry_Value() {
		return (EReference)bookingToRoomMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomTypeToIntegerMapEntry() {
		return roomTypeToIntegerMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomTypeToIntegerMapEntry_Key() {
		return (EReference)roomTypeToIntegerMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomTypeToIntegerMapEntry_Value() {
		return (EAttribute)roomTypeToIntegerMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomToExtraMapEntry() {
		return roomToExtraMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomToExtraMapEntry_Key() {
		return (EReference)roomToExtraMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomToExtraMapEntry_Value() {
		return (EReference)roomToExtraMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtra() {
		return extraEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtra_Description() {
		return (EAttribute)extraEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtra_Price() {
		return (EAttribute)extraEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntitiesFactory getEntitiesFactory() {
		return (EntitiesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		creditCardEClass = createEClass(CREDIT_CARD);
		createEAttribute(creditCardEClass, CREDIT_CARD__CC_NUMBER);
		createEAttribute(creditCardEClass, CREDIT_CARD__CCV);
		createEAttribute(creditCardEClass, CREDIT_CARD__EXPIRY_MONTH);
		createEAttribute(creditCardEClass, CREDIT_CARD__EXPIRY_YEAR);
		createEAttribute(creditCardEClass, CREDIT_CARD__FIRST_NAME);
		createEAttribute(creditCardEClass, CREDIT_CARD__LAST_NAME);

		roomTypeToRoomMapEntryEClass = createEClass(ROOM_TYPE_TO_ROOM_MAP_ENTRY);
		createEReference(roomTypeToRoomMapEntryEClass, ROOM_TYPE_TO_ROOM_MAP_ENTRY__KEY);
		createEReference(roomTypeToRoomMapEntryEClass, ROOM_TYPE_TO_ROOM_MAP_ENTRY__VALUE);

		bookingToRoomMapEntryEClass = createEClass(BOOKING_TO_ROOM_MAP_ENTRY);
		createEReference(bookingToRoomMapEntryEClass, BOOKING_TO_ROOM_MAP_ENTRY__KEY);
		createEReference(bookingToRoomMapEntryEClass, BOOKING_TO_ROOM_MAP_ENTRY__VALUE);

		roomTypeToIntegerMapEntryEClass = createEClass(ROOM_TYPE_TO_INTEGER_MAP_ENTRY);
		createEReference(roomTypeToIntegerMapEntryEClass, ROOM_TYPE_TO_INTEGER_MAP_ENTRY__KEY);
		createEAttribute(roomTypeToIntegerMapEntryEClass, ROOM_TYPE_TO_INTEGER_MAP_ENTRY__VALUE);

		roomToExtraMapEntryEClass = createEClass(ROOM_TO_EXTRA_MAP_ENTRY);
		createEReference(roomToExtraMapEntryEClass, ROOM_TO_EXTRA_MAP_ENTRY__KEY);
		createEReference(roomToExtraMapEntryEClass, ROOM_TO_EXTRA_MAP_ENTRY__VALUE);

		extraEClass = createEClass(EXTRA);
		createEAttribute(extraEClass, EXTRA__DESCRIPTION);
		createEAttribute(extraEClass, EXTRA__PRICE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RoomTypePackage theRoomTypePackage = (RoomTypePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI);
		RoomPackage theRoomPackage = (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);
		BookingPackage theBookingPackage = (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(creditCardEClass, CreditCard.class, "CreditCard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreditCard_CcNumber(), ecorePackage.getEString(), "ccNumber", null, 1, 1, CreditCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCreditCard_Ccv(), ecorePackage.getEString(), "ccv", null, 1, 1, CreditCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCreditCard_ExpiryMonth(), ecorePackage.getEInt(), "expiryMonth", null, 1, 1, CreditCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCreditCard_ExpiryYear(), ecorePackage.getEInt(), "expiryYear", null, 1, 1, CreditCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCreditCard_FirstName(), ecorePackage.getEString(), "firstName", null, 1, 1, CreditCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCreditCard_LastName(), ecorePackage.getEString(), "lastName", null, 1, 1, CreditCard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomTypeToRoomMapEntryEClass, Map.Entry.class, "RoomTypeToRoomMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomTypeToRoomMapEntry_Key(), theRoomTypePackage.getRoomType(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoomTypeToRoomMapEntry_Value(), theRoomPackage.getRoom(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(bookingToRoomMapEntryEClass, Map.Entry.class, "BookingToRoomMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookingToRoomMapEntry_Key(), theBookingPackage.getBooking(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBookingToRoomMapEntry_Value(), theRoomPackage.getRoom(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomTypeToIntegerMapEntryEClass, Map.Entry.class, "RoomTypeToIntegerMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomTypeToIntegerMapEntry_Key(), theRoomTypePackage.getRoomType(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomTypeToIntegerMapEntry_Value(), ecorePackage.getEIntegerObject(), "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomToExtraMapEntryEClass, Map.Entry.class, "RoomToExtraMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomToExtraMapEntry_Key(), theRoomPackage.getRoom(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoomToExtraMapEntry_Value(), this.getExtra(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(extraEClass, Extra.class, "Extra", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExtra_Description(), ecorePackage.getEString(), "description", null, 1, 1, Extra.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getExtra_Price(), ecorePackage.getEDouble(), "price", null, 1, 1, Extra.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
	}

} //EntitiesPackageImpl
