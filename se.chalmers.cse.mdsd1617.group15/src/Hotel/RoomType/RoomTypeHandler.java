/**
 */
package Hotel.RoomType;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import Hotel.Entities.Extra;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.RoomType.RoomTypePackage#getRoomTypeHandler()
 * @model
 * @generated
 */
public interface RoomTypeHandler extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" numberOfBedsRequired="true" numberOfBedsOrdered="false" extrasMany="true" extrasOrdered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	boolean addRoomType(String name, double price, int numberOfBeds, EList<Extra> extras, String description);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeToChangeRequired="true" roomTypeToChangeOrdered="false" newNameRequired="true" newNameOrdered="false" newPriceRequired="true" newPriceOrdered="false" newNumberOfBedsRequired="true" newNumberOfBedsOrdered="false" newExtrasMany="true" newExtrasOrdered="false" newDescriptionRequired="true" newDescriptionOrdered="false"
	 * @generated
	 */
	boolean updateRoomType(String roomTypeToChange, String newName, double newPrice, int newNumberOfBeds, EList<Extra> newExtras, String newDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	boolean removeRoomType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	boolean changeRoomType(int roomNumber, String roomTypeDescription);

} // RoomTypeHandler
