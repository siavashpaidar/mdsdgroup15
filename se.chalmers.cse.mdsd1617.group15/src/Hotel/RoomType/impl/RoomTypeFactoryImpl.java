/**
 */
package Hotel.RoomType.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import Hotel.RoomType.RoomType;
import Hotel.RoomType.RoomTypeBooker;
import Hotel.RoomType.RoomTypeFactory;
import Hotel.RoomType.RoomTypeHandler;
import Hotel.RoomType.RoomTypeInterface;
import Hotel.RoomType.RoomTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomTypeFactoryImpl extends EFactoryImpl implements RoomTypeFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RoomTypeFactory init() {
		try {
			RoomTypeFactory theRoomTypeFactory = (RoomTypeFactory)EPackage.Registry.INSTANCE.getEFactory(RoomTypePackage.eNS_URI);
			if (theRoomTypeFactory != null) {
				return theRoomTypeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RoomTypeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RoomTypePackage.ROOM_TYPE_HANDLER: return createRoomTypeHandler();
			case RoomTypePackage.ROOM_TYPE_BOOKER: return createRoomTypeBooker();
			case RoomTypePackage.ROOM_TYPE_INTERFACE: return createRoomTypeInterface();
			case RoomTypePackage.ROOM_TYPE: return createRoomType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeHandler createRoomTypeHandler() {
		RoomTypeHandlerImpl roomTypeHandler = new RoomTypeHandlerImpl();
		return roomTypeHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeBooker createRoomTypeBooker() {
		RoomTypeBookerImpl roomTypeBooker = new RoomTypeBookerImpl();
		return roomTypeBooker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomTypeInterface createRoomTypeInterface() {
		return (RoomTypeInterface) RoomTypeInterfaceImpl.basicGetInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomType createRoomType() {
		RoomTypeImpl roomType = new RoomTypeImpl();
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypePackage getRoomTypePackage() {
		return (RoomTypePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RoomTypePackage getPackage() {
		return RoomTypePackage.eINSTANCE;
	}

} //RoomTypeFactoryImpl
