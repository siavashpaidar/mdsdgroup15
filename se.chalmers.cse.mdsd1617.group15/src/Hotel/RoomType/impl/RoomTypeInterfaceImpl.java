/**
 */
package Hotel.RoomType.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import Hotel.RoomType.IRoomTypeInterface;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.RoomTypeBooker;
import Hotel.RoomType.RoomTypeHandler;
import Hotel.RoomType.RoomTypeInterface;
import Hotel.RoomType.RoomTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Hotel.RoomType.impl.RoomTypeInterfaceImpl#getRoomTypes <em>Room Types</em>}</li>
 *   <li>{@link Hotel.RoomType.impl.RoomTypeInterfaceImpl#getRoomTypeBooker <em>Room Type Booker</em>}</li>
 *   <li>{@link Hotel.RoomType.impl.RoomTypeInterfaceImpl#getRoomTypeHandler <em>Room Type Handler</em>}</li>
 *   <li>{@link Hotel.RoomType.impl.RoomTypeInterfaceImpl#getInstance <em>Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomTypeInterfaceImpl extends MinimalEObjectImpl.Container implements RoomTypeInterface {
	/**
	 * The cached value of the '{@link #getRoomTypes() <em>Room Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomType> roomTypes;

	/**
	 * The cached value of the '{@link #getRoomTypeBooker() <em>Room Type Booker</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeBooker()
	 * @generated
	 * @ordered
	 */
	protected RoomTypeBooker roomTypeBooker;

	/**
	 * The cached value of the '{@link #getRoomTypeHandler() <em>Room Type Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeHandler()
	 * @generated
	 * @ordered
	 */
	protected RoomTypeHandler roomTypeHandler;

	/**
	 * The cached value of the '{@link #getInstance() <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance()
	 * @generated NOT
	 * @ordered
	 */
	private static IRoomTypeInterface instance;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private RoomTypeInterfaceImpl() {
		super();
		roomTypes = new BasicEList<RoomType>();
		roomTypeBooker = new RoomTypeBookerImpl();
		roomTypeHandler = new RoomTypeHandlerImpl();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomTypePackage.Literals.ROOM_TYPE_INTERFACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomType> getRoomTypes() {
		if (roomTypes == null) {
			roomTypes = new EObjectResolvingEList<RoomType>(RoomType.class, this, RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPES);
		}
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeBooker getRoomTypeBooker() {
		if (roomTypeBooker != null && roomTypeBooker.eIsProxy()) {
			InternalEObject oldRoomTypeBooker = (InternalEObject)roomTypeBooker;
			roomTypeBooker = (RoomTypeBooker)eResolveProxy(oldRoomTypeBooker);
			if (roomTypeBooker != oldRoomTypeBooker) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER, oldRoomTypeBooker, roomTypeBooker));
			}
		}
		return roomTypeBooker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeBooker basicGetRoomTypeBooker() {
		return roomTypeBooker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeBooker(RoomTypeBooker newRoomTypeBooker) {
		RoomTypeBooker oldRoomTypeBooker = roomTypeBooker;
		roomTypeBooker = newRoomTypeBooker;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER, oldRoomTypeBooker, roomTypeBooker));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeHandler getRoomTypeHandler() {
		if (roomTypeHandler != null && roomTypeHandler.eIsProxy()) {
			InternalEObject oldRoomTypeHandler = (InternalEObject)roomTypeHandler;
			roomTypeHandler = (RoomTypeHandler)eResolveProxy(oldRoomTypeHandler);
			if (roomTypeHandler != oldRoomTypeHandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER, oldRoomTypeHandler, roomTypeHandler));
			}
		}
		return roomTypeHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeHandler basicGetRoomTypeHandler() {
		return roomTypeHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeHandler(RoomTypeHandler newRoomTypeHandler) {
		RoomTypeHandler oldRoomTypeHandler = roomTypeHandler;
		roomTypeHandler = newRoomTypeHandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER, oldRoomTypeHandler, roomTypeHandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IRoomTypeInterface getInstance() {
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public static IRoomTypeInterface basicGetInstance() {
		if(instance == null){
			instance = new RoomTypeInterfaceImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstance(IRoomTypeInterface newInstance) {
		IRoomTypeInterface oldInstance = instance;
		instance = newInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomTypePackage.ROOM_TYPE_INTERFACE__INSTANCE, oldInstance, instance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearComponent() {
		roomTypes.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomType getRoomTypeFromDescription(String description) {
		for(RoomType roomType : roomTypes){
			if(roomType.getDescription() != null && roomType.getDescription().equals(description)){
				return roomType;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPES:
				return getRoomTypes();
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER:
				if (resolve) return getRoomTypeBooker();
				return basicGetRoomTypeBooker();
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER:
				if (resolve) return getRoomTypeHandler();
				return basicGetRoomTypeHandler();
			case RoomTypePackage.ROOM_TYPE_INTERFACE__INSTANCE:
				if (resolve) return getInstance();
				return basicGetInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPES:
				getRoomTypes().clear();
				getRoomTypes().addAll((Collection<? extends RoomType>)newValue);
				return;
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER:
				setRoomTypeBooker((RoomTypeBooker)newValue);
				return;
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER:
				setRoomTypeHandler((RoomTypeHandler)newValue);
				return;
			case RoomTypePackage.ROOM_TYPE_INTERFACE__INSTANCE:
				setInstance((IRoomTypeInterface)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPES:
				getRoomTypes().clear();
				return;
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER:
				setRoomTypeBooker((RoomTypeBooker)null);
				return;
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER:
				setRoomTypeHandler((RoomTypeHandler)null);
				return;
			case RoomTypePackage.ROOM_TYPE_INTERFACE__INSTANCE:
				setInstance((IRoomTypeInterface)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPES:
				return roomTypes != null && !roomTypes.isEmpty();
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER:
				return roomTypeBooker != null;
			case RoomTypePackage.ROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER:
				return roomTypeHandler != null;
			case RoomTypePackage.ROOM_TYPE_INTERFACE__INSTANCE:
				return instance != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomTypePackage.ROOM_TYPE_INTERFACE___CLEAR_COMPONENT:
				clearComponent();
				return null;
			case RoomTypePackage.ROOM_TYPE_INTERFACE___GET_ROOM_TYPE_FROM_DESCRIPTION__STRING:
				return getRoomTypeFromDescription((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomTypeInterfaceImpl
