/**
 */
package Hotel.RoomType.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Entities.Extra;
import Hotel.RoomType.RoomTypeHandler;
import Hotel.RoomType.RoomTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RoomTypeHandlerImpl extends MinimalEObjectImpl.Container implements RoomTypeHandler {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomTypeHandlerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomTypePackage.Literals.ROOM_TYPE_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(String name, double price, int numberOfBeds, EList<Extra> extras, String description) {
		RoomTypeImpl roomType = new RoomTypeImpl();
		roomType.setName(name);
		roomType.setPrice(price);
		roomType.setNumberOfBeds(numberOfBeds);
		roomType.extras = extras;	//Add setter instead
		roomType.setDescription(description);
		RoomTypeInterfaceImpl.basicGetInstance().getRoomTypes().add(roomType);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean updateRoomType(String roomTypeToChange, String newName, double newPrice, int newNumberOfBeds, EList<Extra> newExtras, String newDescription) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean removeRoomType(String name) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean changeRoomType(int roomNumber, String roomTypeDescription) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomTypePackage.ROOM_TYPE_HANDLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_ELIST_STRING:
				return addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (EList<Extra>)arguments.get(3), (String)arguments.get(4));
			case RoomTypePackage.ROOM_TYPE_HANDLER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_ELIST_STRING:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2), (Integer)arguments.get(3), (EList<Extra>)arguments.get(4), (String)arguments.get(5));
			case RoomTypePackage.ROOM_TYPE_HANDLER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_HANDLER___CHANGE_ROOM_TYPE__INT_STRING:
				return changeRoomType((Integer)arguments.get(0), (String)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomTypeHandlerImpl
