/**
 */
package Hotel.RoomType;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.RoomType.RoomTypeInterface#getInstance <em>Instance</em>}</li>
 * </ul>
 *
 * @see Hotel.RoomType.RoomTypePackage#getRoomTypeInterface()
 * @model
 * @generated
 */
public interface RoomTypeInterface extends IRoomTypeInterface {
	/**
	 * Returns the value of the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' reference.
	 * @see #setInstance(IRoomTypeInterface)
	 * @see Hotel.RoomType.RoomTypePackage#getRoomTypeInterface_Instance()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeInterface getInstance();

	/**
	 * Sets the value of the '{@link Hotel.RoomType.RoomTypeInterface#getInstance <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance</em>' reference.
	 * @see #getInstance()
	 * @generated
	 */
	void setInstance(IRoomTypeInterface value);

} // RoomTypeInterface
