/**
 */
package Hotel.RoomType;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Type Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.RoomType.IRoomTypeInterface#getRoomTypes <em>Room Types</em>}</li>
 *   <li>{@link Hotel.RoomType.IRoomTypeInterface#getRoomTypeBooker <em>Room Type Booker</em>}</li>
 *   <li>{@link Hotel.RoomType.IRoomTypeInterface#getRoomTypeHandler <em>Room Type Handler</em>}</li>
 * </ul>
 *
 * @see Hotel.RoomType.RoomTypePackage#getIRoomTypeInterface()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomTypeInterface extends EObject {
	/**
	 * Returns the value of the '<em><b>Room Types</b></em>' reference list.
	 * The list contents are of type {@link Hotel.RoomType.RoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Types</em>' reference list.
	 * @see Hotel.RoomType.RoomTypePackage#getIRoomTypeInterface_RoomTypes()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypes();

	/**
	 * Returns the value of the '<em><b>Room Type Booker</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Booker</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Booker</em>' reference.
	 * @see #setRoomTypeBooker(RoomTypeBooker)
	 * @see Hotel.RoomType.RoomTypePackage#getIRoomTypeInterface_RoomTypeBooker()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomTypeBooker getRoomTypeBooker();

	/**
	 * Sets the value of the '{@link Hotel.RoomType.IRoomTypeInterface#getRoomTypeBooker <em>Room Type Booker</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Booker</em>' reference.
	 * @see #getRoomTypeBooker()
	 * @generated
	 */
	void setRoomTypeBooker(RoomTypeBooker value);

	/**
	 * Returns the value of the '<em><b>Room Type Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Handler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Handler</em>' reference.
	 * @see #setRoomTypeHandler(RoomTypeHandler)
	 * @see Hotel.RoomType.RoomTypePackage#getIRoomTypeInterface_RoomTypeHandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomTypeHandler getRoomTypeHandler();

	/**
	 * Sets the value of the '{@link Hotel.RoomType.IRoomTypeInterface#getRoomTypeHandler <em>Room Type Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Handler</em>' reference.
	 * @see #getRoomTypeHandler()
	 * @generated
	 */
	void setRoomTypeHandler(RoomTypeHandler value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearComponent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	RoomType getRoomTypeFromDescription(String description);

} // IRoomTypeInterface
