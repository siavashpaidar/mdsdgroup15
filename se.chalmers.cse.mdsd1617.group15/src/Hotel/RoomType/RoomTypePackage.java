/**
 */
package Hotel.RoomType;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Hotel.RoomType.RoomTypeFactory
 * @model kind="package"
 * @generated
 */
public interface RoomTypePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "RoomType";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Hotel/RoomType.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Hotel.RoomType";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomTypePackage eINSTANCE = Hotel.RoomType.impl.RoomTypePackageImpl.init();

	/**
	 * The meta object id for the '{@link Hotel.RoomType.impl.RoomTypeHandlerImpl <em>Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.RoomType.impl.RoomTypeHandlerImpl
	 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getRoomTypeHandler()
	 * @generated
	 */
	int ROOM_TYPE_HANDLER = 0;

	/**
	 * The number of structural features of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_ELIST_STRING = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_ELIST_STRING = 1;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER___REMOVE_ROOM_TYPE__STRING = 2;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER___CHANGE_ROOM_TYPE__INT_STRING = 3;

	/**
	 * The number of operations of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link Hotel.RoomType.impl.RoomTypeBookerImpl <em>Booker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.RoomType.impl.RoomTypeBookerImpl
	 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getRoomTypeBooker()
	 * @generated
	 */
	int ROOM_TYPE_BOOKER = 1;

	/**
	 * The number of structural features of the '<em>Booker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_BOOKER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Book Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_BOOKER___BOOK_ROOM__STRING_STRING_STRING = 0;

	/**
	 * The number of operations of the '<em>Booker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_BOOKER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link Hotel.RoomType.IRoomTypeInterface <em>IRoom Type Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.RoomType.IRoomTypeInterface
	 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getIRoomTypeInterface()
	 * @generated
	 */
	int IROOM_TYPE_INTERFACE = 2;

	/**
	 * The feature id for the '<em><b>Room Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_INTERFACE__ROOM_TYPES = 0;

	/**
	 * The feature id for the '<em><b>Room Type Booker</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER = 1;

	/**
	 * The feature id for the '<em><b>Room Type Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER = 2;

	/**
	 * The number of structural features of the '<em>IRoom Type Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_INTERFACE_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Clear Component</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_INTERFACE___CLEAR_COMPONENT = 0;

	/**
	 * The operation id for the '<em>Get Room Type From Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_INTERFACE___GET_ROOM_TYPE_FROM_DESCRIPTION__STRING = 1;

	/**
	 * The number of operations of the '<em>IRoom Type Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_INTERFACE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link Hotel.RoomType.impl.RoomTypeInterfaceImpl <em>Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.RoomType.impl.RoomTypeInterfaceImpl
	 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getRoomTypeInterface()
	 * @generated
	 */
	int ROOM_TYPE_INTERFACE = 3;

	/**
	 * The feature id for the '<em><b>Room Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_INTERFACE__ROOM_TYPES = IROOM_TYPE_INTERFACE__ROOM_TYPES;

	/**
	 * The feature id for the '<em><b>Room Type Booker</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER = IROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER;

	/**
	 * The feature id for the '<em><b>Room Type Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER = IROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_INTERFACE__INSTANCE = IROOM_TYPE_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_INTERFACE_FEATURE_COUNT = IROOM_TYPE_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Clear Component</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_INTERFACE___CLEAR_COMPONENT = IROOM_TYPE_INTERFACE___CLEAR_COMPONENT;

	/**
	 * The operation id for the '<em>Get Room Type From Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_INTERFACE___GET_ROOM_TYPE_FROM_DESCRIPTION__STRING = IROOM_TYPE_INTERFACE___GET_ROOM_TYPE_FROM_DESCRIPTION__STRING;

	/**
	 * The number of operations of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_INTERFACE_OPERATION_COUNT = IROOM_TYPE_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Hotel.RoomType.impl.RoomTypeImpl <em>Room Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.RoomType.impl.RoomTypeImpl
	 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__PRICE = 1;

	/**
	 * The feature id for the '<em><b>Number Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NUMBER_OF_BEDS = 2;

	/**
	 * The feature id for the '<em><b>Extras</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__EXTRAS = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__DESCRIPTION = 4;

	/**
	 * The number of structural features of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link Hotel.RoomType.RoomTypeHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Handler</em>'.
	 * @see Hotel.RoomType.RoomTypeHandler
	 * @generated
	 */
	EClass getRoomTypeHandler();

	/**
	 * Returns the meta object for the '{@link Hotel.RoomType.RoomTypeHandler#addRoomType(java.lang.String, double, int, org.eclipse.emf.common.util.EList, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see Hotel.RoomType.RoomTypeHandler#addRoomType(java.lang.String, double, int, org.eclipse.emf.common.util.EList, java.lang.String)
	 * @generated
	 */
	EOperation getRoomTypeHandler__AddRoomType__String_double_int_EList_String();

	/**
	 * Returns the meta object for the '{@link Hotel.RoomType.RoomTypeHandler#updateRoomType(java.lang.String, java.lang.String, double, int, org.eclipse.emf.common.util.EList, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see Hotel.RoomType.RoomTypeHandler#updateRoomType(java.lang.String, java.lang.String, double, int, org.eclipse.emf.common.util.EList, java.lang.String)
	 * @generated
	 */
	EOperation getRoomTypeHandler__UpdateRoomType__String_String_double_int_EList_String();

	/**
	 * Returns the meta object for the '{@link Hotel.RoomType.RoomTypeHandler#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see Hotel.RoomType.RoomTypeHandler#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getRoomTypeHandler__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link Hotel.RoomType.RoomTypeHandler#changeRoomType(int, java.lang.String) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see Hotel.RoomType.RoomTypeHandler#changeRoomType(int, java.lang.String)
	 * @generated
	 */
	EOperation getRoomTypeHandler__ChangeRoomType__int_String();

	/**
	 * Returns the meta object for class '{@link Hotel.RoomType.RoomTypeBooker <em>Booker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booker</em>'.
	 * @see Hotel.RoomType.RoomTypeBooker
	 * @generated
	 */
	EClass getRoomTypeBooker();

	/**
	 * Returns the meta object for the '{@link Hotel.RoomType.RoomTypeBooker#bookRoom(java.lang.String, java.lang.String, java.lang.String) <em>Book Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Book Room</em>' operation.
	 * @see Hotel.RoomType.RoomTypeBooker#bookRoom(java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getRoomTypeBooker__BookRoom__String_String_String();

	/**
	 * Returns the meta object for class '{@link Hotel.RoomType.IRoomTypeInterface <em>IRoom Type Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Interface</em>'.
	 * @see Hotel.RoomType.IRoomTypeInterface
	 * @generated
	 */
	EClass getIRoomTypeInterface();

	/**
	 * Returns the meta object for the reference list '{@link Hotel.RoomType.IRoomTypeInterface#getRoomTypes <em>Room Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Room Types</em>'.
	 * @see Hotel.RoomType.IRoomTypeInterface#getRoomTypes()
	 * @see #getIRoomTypeInterface()
	 * @generated
	 */
	EReference getIRoomTypeInterface_RoomTypes();

	/**
	 * Returns the meta object for the reference '{@link Hotel.RoomType.IRoomTypeInterface#getRoomTypeBooker <em>Room Type Booker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type Booker</em>'.
	 * @see Hotel.RoomType.IRoomTypeInterface#getRoomTypeBooker()
	 * @see #getIRoomTypeInterface()
	 * @generated
	 */
	EReference getIRoomTypeInterface_RoomTypeBooker();

	/**
	 * Returns the meta object for the reference '{@link Hotel.RoomType.IRoomTypeInterface#getRoomTypeHandler <em>Room Type Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type Handler</em>'.
	 * @see Hotel.RoomType.IRoomTypeInterface#getRoomTypeHandler()
	 * @see #getIRoomTypeInterface()
	 * @generated
	 */
	EReference getIRoomTypeInterface_RoomTypeHandler();

	/**
	 * Returns the meta object for the '{@link Hotel.RoomType.IRoomTypeInterface#clearComponent() <em>Clear Component</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Component</em>' operation.
	 * @see Hotel.RoomType.IRoomTypeInterface#clearComponent()
	 * @generated
	 */
	EOperation getIRoomTypeInterface__ClearComponent();

	/**
	 * Returns the meta object for the '{@link Hotel.RoomType.IRoomTypeInterface#getRoomTypeFromDescription(java.lang.String) <em>Get Room Type From Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type From Description</em>' operation.
	 * @see Hotel.RoomType.IRoomTypeInterface#getRoomTypeFromDescription(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeInterface__GetRoomTypeFromDescription__String();

	/**
	 * Returns the meta object for class '{@link Hotel.RoomType.RoomTypeInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface</em>'.
	 * @see Hotel.RoomType.RoomTypeInterface
	 * @generated
	 */
	EClass getRoomTypeInterface();

	/**
	 * Returns the meta object for the reference '{@link Hotel.RoomType.RoomTypeInterface#getInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Instance</em>'.
	 * @see Hotel.RoomType.RoomTypeInterface#getInstance()
	 * @see #getRoomTypeInterface()
	 * @generated
	 */
	EReference getRoomTypeInterface_Instance();

	/**
	 * Returns the meta object for class '{@link Hotel.RoomType.RoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type</em>'.
	 * @see Hotel.RoomType.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.RoomType.RoomType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see Hotel.RoomType.RoomType#getName()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Name();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.RoomType.RoomType#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see Hotel.RoomType.RoomType#getPrice()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Price();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.RoomType.RoomType#getNumberOfBeds <em>Number Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Beds</em>'.
	 * @see Hotel.RoomType.RoomType#getNumberOfBeds()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_NumberOfBeds();

	/**
	 * Returns the meta object for the reference list '{@link Hotel.RoomType.RoomType#getExtras <em>Extras</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extras</em>'.
	 * @see Hotel.RoomType.RoomType#getExtras()
	 * @see #getRoomType()
	 * @generated
	 */
	EReference getRoomType_Extras();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.RoomType.RoomType#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see Hotel.RoomType.RoomType#getDescription()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Description();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomTypeFactory getRoomTypeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Hotel.RoomType.impl.RoomTypeHandlerImpl <em>Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.RoomType.impl.RoomTypeHandlerImpl
		 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getRoomTypeHandler()
		 * @generated
		 */
		EClass ROOM_TYPE_HANDLER = eINSTANCE.getRoomTypeHandler();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_TYPE_HANDLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_ELIST_STRING = eINSTANCE.getRoomTypeHandler__AddRoomType__String_double_int_EList_String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_TYPE_HANDLER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_ELIST_STRING = eINSTANCE.getRoomTypeHandler__UpdateRoomType__String_String_double_int_EList_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_TYPE_HANDLER___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getRoomTypeHandler__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_TYPE_HANDLER___CHANGE_ROOM_TYPE__INT_STRING = eINSTANCE.getRoomTypeHandler__ChangeRoomType__int_String();

		/**
		 * The meta object literal for the '{@link Hotel.RoomType.impl.RoomTypeBookerImpl <em>Booker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.RoomType.impl.RoomTypeBookerImpl
		 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getRoomTypeBooker()
		 * @generated
		 */
		EClass ROOM_TYPE_BOOKER = eINSTANCE.getRoomTypeBooker();

		/**
		 * The meta object literal for the '<em><b>Book Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_TYPE_BOOKER___BOOK_ROOM__STRING_STRING_STRING = eINSTANCE.getRoomTypeBooker__BookRoom__String_String_String();

		/**
		 * The meta object literal for the '{@link Hotel.RoomType.IRoomTypeInterface <em>IRoom Type Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.RoomType.IRoomTypeInterface
		 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getIRoomTypeInterface()
		 * @generated
		 */
		EClass IROOM_TYPE_INTERFACE = eINSTANCE.getIRoomTypeInterface();

		/**
		 * The meta object literal for the '<em><b>Room Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IROOM_TYPE_INTERFACE__ROOM_TYPES = eINSTANCE.getIRoomTypeInterface_RoomTypes();

		/**
		 * The meta object literal for the '<em><b>Room Type Booker</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IROOM_TYPE_INTERFACE__ROOM_TYPE_BOOKER = eINSTANCE.getIRoomTypeInterface_RoomTypeBooker();

		/**
		 * The meta object literal for the '<em><b>Room Type Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IROOM_TYPE_INTERFACE__ROOM_TYPE_HANDLER = eINSTANCE.getIRoomTypeInterface_RoomTypeHandler();

		/**
		 * The meta object literal for the '<em><b>Clear Component</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_INTERFACE___CLEAR_COMPONENT = eINSTANCE.getIRoomTypeInterface__ClearComponent();

		/**
		 * The meta object literal for the '<em><b>Get Room Type From Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_INTERFACE___GET_ROOM_TYPE_FROM_DESCRIPTION__STRING = eINSTANCE.getIRoomTypeInterface__GetRoomTypeFromDescription__String();

		/**
		 * The meta object literal for the '{@link Hotel.RoomType.impl.RoomTypeInterfaceImpl <em>Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.RoomType.impl.RoomTypeInterfaceImpl
		 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getRoomTypeInterface()
		 * @generated
		 */
		EClass ROOM_TYPE_INTERFACE = eINSTANCE.getRoomTypeInterface();

		/**
		 * The meta object literal for the '<em><b>Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_INTERFACE__INSTANCE = eINSTANCE.getRoomTypeInterface_Instance();

		/**
		 * The meta object literal for the '{@link Hotel.RoomType.impl.RoomTypeImpl <em>Room Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.RoomType.impl.RoomTypeImpl
		 * @see Hotel.RoomType.impl.RoomTypePackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NAME = eINSTANCE.getRoomType_Name();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__PRICE = eINSTANCE.getRoomType_Price();

		/**
		 * The meta object literal for the '<em><b>Number Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NUMBER_OF_BEDS = eINSTANCE.getRoomType_NumberOfBeds();

		/**
		 * The meta object literal for the '<em><b>Extras</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE__EXTRAS = eINSTANCE.getRoomType_Extras();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__DESCRIPTION = eINSTANCE.getRoomType_Description();

	}

} //RoomTypePackage
