/**
 */
package Hotel.RoomType;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booker</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.RoomType.RoomTypePackage#getRoomTypeBooker()
 * @model
 * @generated
 */
public interface RoomTypeBooker extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" startTimeRequired="true" startTimeOrdered="false" endTimeRequired="true" endTimeOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	boolean bookRoom(String startTime, String endTime, String roomTypeDescription);

} // RoomTypeBooker
