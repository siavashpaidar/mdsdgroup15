/**
 */
package Hotel.RoomType.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import Hotel.RoomType.IRoomTypeInterface;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.RoomTypeBooker;
import Hotel.RoomType.RoomTypeHandler;
import Hotel.RoomType.RoomTypeInterface;
import Hotel.RoomType.RoomTypePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see Hotel.RoomType.RoomTypePackage
 * @generated
 */
public class RoomTypeAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RoomTypePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = RoomTypePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomTypeSwitch<Adapter> modelSwitch =
		new RoomTypeSwitch<Adapter>() {
			@Override
			public Adapter caseRoomTypeHandler(RoomTypeHandler object) {
				return createRoomTypeHandlerAdapter();
			}
			@Override
			public Adapter caseRoomTypeBooker(RoomTypeBooker object) {
				return createRoomTypeBookerAdapter();
			}
			@Override
			public Adapter caseIRoomTypeInterface(IRoomTypeInterface object) {
				return createIRoomTypeInterfaceAdapter();
			}
			@Override
			public Adapter caseRoomTypeInterface(RoomTypeInterface object) {
				return createRoomTypeInterfaceAdapter();
			}
			@Override
			public Adapter caseRoomType(RoomType object) {
				return createRoomTypeAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link Hotel.RoomType.RoomTypeHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Hotel.RoomType.RoomTypeHandler
	 * @generated
	 */
	public Adapter createRoomTypeHandlerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Hotel.RoomType.RoomTypeBooker <em>Booker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Hotel.RoomType.RoomTypeBooker
	 * @generated
	 */
	public Adapter createRoomTypeBookerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Hotel.RoomType.IRoomTypeInterface <em>IRoom Type Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Hotel.RoomType.IRoomTypeInterface
	 * @generated
	 */
	public Adapter createIRoomTypeInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Hotel.RoomType.RoomTypeInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Hotel.RoomType.RoomTypeInterface
	 * @generated
	 */
	public Adapter createRoomTypeInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Hotel.RoomType.RoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Hotel.RoomType.RoomType
	 * @generated
	 */
	public Adapter createRoomTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //RoomTypeAdapterFactory
