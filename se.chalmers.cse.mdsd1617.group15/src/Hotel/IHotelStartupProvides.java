/**
 */
package Hotel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.HotelPackage#getIHotelStartupProvides()
 * @model
 * @generated
 */
public interface IHotelStartupProvides extends IHotelStartupProvidesInterface {
} // IHotelStartupProvides
