/**
 */
package Hotel.Room;

import org.eclipse.emf.ecore.EObject;

import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Room.Room#getRoomID <em>Room ID</em>}</li>
 *   <li>{@link Hotel.Room.Room#getRoomType <em>Room Type</em>}</li>
 *   <li>{@link Hotel.Room.Room#getRoomStatus <em>Room Status</em>}</li>
 * </ul>
 *
 * @see Hotel.Room.RoomPackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends EObject {
	/**
	 * Returns the value of the '<em><b>Room ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room ID</em>' attribute.
	 * @see #setRoomID(int)
	 * @see Hotel.Room.RoomPackage#getRoom_RoomID()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getRoomID();

	/**
	 * Sets the value of the '{@link Hotel.Room.Room#getRoomID <em>Room ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room ID</em>' attribute.
	 * @see #getRoomID()
	 * @generated
	 */
	void setRoomID(int value);

	/**
	 * Returns the value of the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type</em>' reference.
	 * @see #setRoomType(RoomType)
	 * @see Hotel.Room.RoomPackage#getRoom_RoomType()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomType getRoomType();

	/**
	 * Sets the value of the '{@link Hotel.Room.Room#getRoomType <em>Room Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type</em>' reference.
	 * @see #getRoomType()
	 * @generated
	 */
	void setRoomType(RoomType value);

	/**
	 * Returns the value of the '<em><b>Room Status</b></em>' attribute.
	 * The literals are from the enumeration {@link Hotel.Room.RoomStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Status</em>' attribute.
	 * @see Hotel.Room.RoomStatus
	 * @see #setRoomStatus(RoomStatus)
	 * @see Hotel.Room.RoomPackage#getRoom_RoomStatus()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomStatus getRoomStatus();

	/**
	 * Sets the value of the '{@link Hotel.Room.Room#getRoomStatus <em>Room Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Status</em>' attribute.
	 * @see Hotel.Room.RoomStatus
	 * @see #getRoomStatus()
	 * @generated
	 */
	void setRoomStatus(RoomStatus value);

} // Room
