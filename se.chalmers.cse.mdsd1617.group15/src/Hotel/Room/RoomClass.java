/**
 */
package Hotel.Room;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.Room.RoomPackage#getRoomClass()
 * @model
 * @generated
 */
public interface RoomClass extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	int checkInRoom(int bookingID, String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean checkOutRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" ccnrRequired="true" ccnrOrdered="false"
	 * @generated
	 */
	boolean payRoom(int bookingID, String ccnr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" roomIDRequired="true" roomIDOrdered="false" extraRequired="true" extraOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	boolean addRoomExtra(int bookingID, String roomID, String extra, double price);

} // RoomClass
