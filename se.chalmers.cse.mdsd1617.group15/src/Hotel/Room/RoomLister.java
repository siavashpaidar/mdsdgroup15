/**
 */
package Hotel.Room;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lister</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.Room.RoomPackage#getRoomLister()
 * @model
 * @generated
 */
public interface RoomLister extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dayRequired="true" dayOrdered="false"
	 * @generated
	 */
	EList<Room> listOccupiedRooms(Date day);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="Hotel.Entities.RoomTypeToRoomMapEntry<Hotel.RoomType.RoomType, Hotel.Room.Room>" ordered="false" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false" minNumberOfBedsRequired="true" minNumberOfBedsOrdered="false"
	 * @generated
	 */
	EMap<RoomType, EList<Room>> searchFreeRooms(Date fromDay, Date toDay, int minNumberOfBeds);

} // RoomLister
