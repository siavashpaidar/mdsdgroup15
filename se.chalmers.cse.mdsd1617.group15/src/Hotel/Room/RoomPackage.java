/**
 */
package Hotel.Room;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Hotel.Room.RoomFactory
 * @model kind="package"
 * @generated
 */
public interface RoomPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Room";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Hotel/Room.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Hotel.Room";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomPackage eINSTANCE = Hotel.Room.impl.RoomPackageImpl.init();

	/**
	 * The meta object id for the '{@link Hotel.Room.impl.RoomClassImpl <em>Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Room.impl.RoomClassImpl
	 * @see Hotel.Room.impl.RoomPackageImpl#getRoomClass()
	 * @generated
	 */
	int ROOM_CLASS = 0;

	/**
	 * The number of structural features of the '<em>Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CLASS_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CLASS___CHECK_IN_ROOM__INT_STRING = 0;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CLASS___CHECK_OUT_ROOM__INT = 1;

	/**
	 * The operation id for the '<em>Pay Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CLASS___PAY_ROOM__INT_STRING = 2;

	/**
	 * The operation id for the '<em>Add Room Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CLASS___ADD_ROOM_EXTRA__INT_STRING_STRING_DOUBLE = 3;

	/**
	 * The number of operations of the '<em>Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_CLASS_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link Hotel.Room.impl.RoomListerImpl <em>Lister</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Room.impl.RoomListerImpl
	 * @see Hotel.Room.impl.RoomPackageImpl#getRoomLister()
	 * @generated
	 */
	int ROOM_LISTER = 1;

	/**
	 * The number of structural features of the '<em>Lister</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_LISTER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_LISTER___LIST_OCCUPIED_ROOMS__DATE = 0;

	/**
	 * The operation id for the '<em>Search Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_LISTER___SEARCH_FREE_ROOMS__DATE_DATE_INT = 1;

	/**
	 * The number of operations of the '<em>Lister</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_LISTER_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link Hotel.Room.impl.RoomModifierImpl <em>Modifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Room.impl.RoomModifierImpl
	 * @see Hotel.Room.impl.RoomPackageImpl#getRoomModifier()
	 * @generated
	 */
	int ROOM_MODIFIER = 2;

	/**
	 * The number of structural features of the '<em>Modifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MODIFIER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MODIFIER___ADD_ROOM__INT_STRING = 0;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MODIFIER___BLOCK_ROOM__INT = 1;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MODIFIER___REMOVE_ROOM__INT = 2;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MODIFIER___UNBLOCK_ROOM__INT = 3;

	/**
	 * The number of operations of the '<em>Modifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MODIFIER_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link Hotel.Room.IRoomInterface <em>IRoom Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Room.IRoomInterface
	 * @see Hotel.Room.impl.RoomPackageImpl#getIRoomInterface()
	 * @generated
	 */
	int IROOM_INTERFACE = 3;

	/**
	 * The feature id for the '<em><b>Room Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_INTERFACE__ROOM_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Room Lister</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_INTERFACE__ROOM_LISTER = 1;

	/**
	 * The feature id for the '<em><b>Room Modifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_INTERFACE__ROOM_MODIFIER = 2;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_INTERFACE__ROOMS = 3;

	/**
	 * The number of structural features of the '<em>IRoom Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_INTERFACE_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Clear Component</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_INTERFACE___CLEAR_COMPONENT = 0;

	/**
	 * The operation id for the '<em>Get Room From Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_INTERFACE___GET_ROOM_FROM_ID__INT = 1;

	/**
	 * The number of operations of the '<em>IRoom Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_INTERFACE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link Hotel.Room.impl.RoomInterfaceImpl <em>Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Room.impl.RoomInterfaceImpl
	 * @see Hotel.Room.impl.RoomPackageImpl#getRoomInterface()
	 * @generated
	 */
	int ROOM_INTERFACE = 4;

	/**
	 * The feature id for the '<em><b>Room Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_INTERFACE__ROOM_CLASS = IROOM_INTERFACE__ROOM_CLASS;

	/**
	 * The feature id for the '<em><b>Room Lister</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_INTERFACE__ROOM_LISTER = IROOM_INTERFACE__ROOM_LISTER;

	/**
	 * The feature id for the '<em><b>Room Modifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_INTERFACE__ROOM_MODIFIER = IROOM_INTERFACE__ROOM_MODIFIER;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_INTERFACE__ROOMS = IROOM_INTERFACE__ROOMS;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_INTERFACE__INSTANCE = IROOM_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_INTERFACE_FEATURE_COUNT = IROOM_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Clear Component</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_INTERFACE___CLEAR_COMPONENT = IROOM_INTERFACE___CLEAR_COMPONENT;

	/**
	 * The operation id for the '<em>Get Room From Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_INTERFACE___GET_ROOM_FROM_ID__INT = IROOM_INTERFACE___GET_ROOM_FROM_ID__INT;

	/**
	 * The number of operations of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_INTERFACE_OPERATION_COUNT = IROOM_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Hotel.Room.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Room.impl.RoomImpl
	 * @see Hotel.Room.impl.RoomPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 5;

	/**
	 * The feature id for the '<em><b>Room ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_ID = 0;

	/**
	 * The feature id for the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Room Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_STATUS = 2;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Hotel.Room.RoomStatus <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Room.RoomStatus
	 * @see Hotel.Room.impl.RoomPackageImpl#getRoomStatus()
	 * @generated
	 */
	int ROOM_STATUS = 6;


	/**
	 * Returns the meta object for class '{@link Hotel.Room.RoomClass <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class</em>'.
	 * @see Hotel.Room.RoomClass
	 * @generated
	 */
	EClass getRoomClass();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomClass#checkInRoom(int, java.lang.String) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see Hotel.Room.RoomClass#checkInRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getRoomClass__CheckInRoom__int_String();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomClass#checkOutRoom(int) <em>Check Out Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Room</em>' operation.
	 * @see Hotel.Room.RoomClass#checkOutRoom(int)
	 * @generated
	 */
	EOperation getRoomClass__CheckOutRoom__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomClass#payRoom(int, java.lang.String) <em>Pay Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room</em>' operation.
	 * @see Hotel.Room.RoomClass#payRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getRoomClass__PayRoom__int_String();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomClass#addRoomExtra(int, java.lang.String, java.lang.String, double) <em>Add Room Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Extra</em>' operation.
	 * @see Hotel.Room.RoomClass#addRoomExtra(int, java.lang.String, java.lang.String, double)
	 * @generated
	 */
	EOperation getRoomClass__AddRoomExtra__int_String_String_double();

	/**
	 * Returns the meta object for class '{@link Hotel.Room.RoomLister <em>Lister</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lister</em>'.
	 * @see Hotel.Room.RoomLister
	 * @generated
	 */
	EClass getRoomLister();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomLister#listOccupiedRooms(java.util.Date) <em>List Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Occupied Rooms</em>' operation.
	 * @see Hotel.Room.RoomLister#listOccupiedRooms(java.util.Date)
	 * @generated
	 */
	EOperation getRoomLister__ListOccupiedRooms__Date();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomLister#searchFreeRooms(java.util.Date, java.util.Date, int) <em>Search Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Search Free Rooms</em>' operation.
	 * @see Hotel.Room.RoomLister#searchFreeRooms(java.util.Date, java.util.Date, int)
	 * @generated
	 */
	EOperation getRoomLister__SearchFreeRooms__Date_Date_int();

	/**
	 * Returns the meta object for class '{@link Hotel.Room.RoomModifier <em>Modifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Modifier</em>'.
	 * @see Hotel.Room.RoomModifier
	 * @generated
	 */
	EClass getRoomModifier();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomModifier#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see Hotel.Room.RoomModifier#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getRoomModifier__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomModifier#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see Hotel.Room.RoomModifier#blockRoom(int)
	 * @generated
	 */
	EOperation getRoomModifier__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomModifier#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see Hotel.Room.RoomModifier#removeRoom(int)
	 * @generated
	 */
	EOperation getRoomModifier__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.RoomModifier#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see Hotel.Room.RoomModifier#unblockRoom(int)
	 * @generated
	 */
	EOperation getRoomModifier__UnblockRoom__int();

	/**
	 * Returns the meta object for class '{@link Hotel.Room.IRoomInterface <em>IRoom Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Interface</em>'.
	 * @see Hotel.Room.IRoomInterface
	 * @generated
	 */
	EClass getIRoomInterface();

	/**
	 * Returns the meta object for the reference '{@link Hotel.Room.IRoomInterface#getRoomClass <em>Room Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Class</em>'.
	 * @see Hotel.Room.IRoomInterface#getRoomClass()
	 * @see #getIRoomInterface()
	 * @generated
	 */
	EReference getIRoomInterface_RoomClass();

	/**
	 * Returns the meta object for the reference '{@link Hotel.Room.IRoomInterface#getRoomLister <em>Room Lister</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Lister</em>'.
	 * @see Hotel.Room.IRoomInterface#getRoomLister()
	 * @see #getIRoomInterface()
	 * @generated
	 */
	EReference getIRoomInterface_RoomLister();

	/**
	 * Returns the meta object for the reference '{@link Hotel.Room.IRoomInterface#getRoomModifier <em>Room Modifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Modifier</em>'.
	 * @see Hotel.Room.IRoomInterface#getRoomModifier()
	 * @see #getIRoomInterface()
	 * @generated
	 */
	EReference getIRoomInterface_RoomModifier();

	/**
	 * Returns the meta object for the reference list '{@link Hotel.Room.IRoomInterface#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms</em>'.
	 * @see Hotel.Room.IRoomInterface#getRooms()
	 * @see #getIRoomInterface()
	 * @generated
	 */
	EReference getIRoomInterface_Rooms();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.IRoomInterface#clearComponent() <em>Clear Component</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Component</em>' operation.
	 * @see Hotel.Room.IRoomInterface#clearComponent()
	 * @generated
	 */
	EOperation getIRoomInterface__ClearComponent();

	/**
	 * Returns the meta object for the '{@link Hotel.Room.IRoomInterface#getRoomFromId(int) <em>Get Room From Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room From Id</em>' operation.
	 * @see Hotel.Room.IRoomInterface#getRoomFromId(int)
	 * @generated
	 */
	EOperation getIRoomInterface__GetRoomFromId__int();

	/**
	 * Returns the meta object for class '{@link Hotel.Room.RoomInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface</em>'.
	 * @see Hotel.Room.RoomInterface
	 * @generated
	 */
	EClass getRoomInterface();

	/**
	 * Returns the meta object for the reference '{@link Hotel.Room.RoomInterface#getInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Instance</em>'.
	 * @see Hotel.Room.RoomInterface#getInstance()
	 * @see #getRoomInterface()
	 * @generated
	 */
	EReference getRoomInterface_Instance();

	/**
	 * Returns the meta object for class '{@link Hotel.Room.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see Hotel.Room.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Room.Room#getRoomID <em>Room ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room ID</em>'.
	 * @see Hotel.Room.Room#getRoomID()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_RoomID();

	/**
	 * Returns the meta object for the reference '{@link Hotel.Room.Room#getRoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type</em>'.
	 * @see Hotel.Room.Room#getRoomType()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_RoomType();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Room.Room#getRoomStatus <em>Room Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Status</em>'.
	 * @see Hotel.Room.Room#getRoomStatus()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_RoomStatus();

	/**
	 * Returns the meta object for enum '{@link Hotel.Room.RoomStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see Hotel.Room.RoomStatus
	 * @generated
	 */
	EEnum getRoomStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomFactory getRoomFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Hotel.Room.impl.RoomClassImpl <em>Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Room.impl.RoomClassImpl
		 * @see Hotel.Room.impl.RoomPackageImpl#getRoomClass()
		 * @generated
		 */
		EClass ROOM_CLASS = eINSTANCE.getRoomClass();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CLASS___CHECK_IN_ROOM__INT_STRING = eINSTANCE.getRoomClass__CheckInRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Check Out Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CLASS___CHECK_OUT_ROOM__INT = eINSTANCE.getRoomClass__CheckOutRoom__int();

		/**
		 * The meta object literal for the '<em><b>Pay Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CLASS___PAY_ROOM__INT_STRING = eINSTANCE.getRoomClass__PayRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Add Room Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_CLASS___ADD_ROOM_EXTRA__INT_STRING_STRING_DOUBLE = eINSTANCE.getRoomClass__AddRoomExtra__int_String_String_double();

		/**
		 * The meta object literal for the '{@link Hotel.Room.impl.RoomListerImpl <em>Lister</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Room.impl.RoomListerImpl
		 * @see Hotel.Room.impl.RoomPackageImpl#getRoomLister()
		 * @generated
		 */
		EClass ROOM_LISTER = eINSTANCE.getRoomLister();

		/**
		 * The meta object literal for the '<em><b>List Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_LISTER___LIST_OCCUPIED_ROOMS__DATE = eINSTANCE.getRoomLister__ListOccupiedRooms__Date();

		/**
		 * The meta object literal for the '<em><b>Search Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_LISTER___SEARCH_FREE_ROOMS__DATE_DATE_INT = eINSTANCE.getRoomLister__SearchFreeRooms__Date_Date_int();

		/**
		 * The meta object literal for the '{@link Hotel.Room.impl.RoomModifierImpl <em>Modifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Room.impl.RoomModifierImpl
		 * @see Hotel.Room.impl.RoomPackageImpl#getRoomModifier()
		 * @generated
		 */
		EClass ROOM_MODIFIER = eINSTANCE.getRoomModifier();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_MODIFIER___ADD_ROOM__INT_STRING = eINSTANCE.getRoomModifier__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_MODIFIER___BLOCK_ROOM__INT = eINSTANCE.getRoomModifier__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_MODIFIER___REMOVE_ROOM__INT = eINSTANCE.getRoomModifier__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_MODIFIER___UNBLOCK_ROOM__INT = eINSTANCE.getRoomModifier__UnblockRoom__int();

		/**
		 * The meta object literal for the '{@link Hotel.Room.IRoomInterface <em>IRoom Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Room.IRoomInterface
		 * @see Hotel.Room.impl.RoomPackageImpl#getIRoomInterface()
		 * @generated
		 */
		EClass IROOM_INTERFACE = eINSTANCE.getIRoomInterface();

		/**
		 * The meta object literal for the '<em><b>Room Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IROOM_INTERFACE__ROOM_CLASS = eINSTANCE.getIRoomInterface_RoomClass();

		/**
		 * The meta object literal for the '<em><b>Room Lister</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IROOM_INTERFACE__ROOM_LISTER = eINSTANCE.getIRoomInterface_RoomLister();

		/**
		 * The meta object literal for the '<em><b>Room Modifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IROOM_INTERFACE__ROOM_MODIFIER = eINSTANCE.getIRoomInterface_RoomModifier();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IROOM_INTERFACE__ROOMS = eINSTANCE.getIRoomInterface_Rooms();

		/**
		 * The meta object literal for the '<em><b>Clear Component</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_INTERFACE___CLEAR_COMPONENT = eINSTANCE.getIRoomInterface__ClearComponent();

		/**
		 * The meta object literal for the '<em><b>Get Room From Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_INTERFACE___GET_ROOM_FROM_ID__INT = eINSTANCE.getIRoomInterface__GetRoomFromId__int();

		/**
		 * The meta object literal for the '{@link Hotel.Room.impl.RoomInterfaceImpl <em>Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Room.impl.RoomInterfaceImpl
		 * @see Hotel.Room.impl.RoomPackageImpl#getRoomInterface()
		 * @generated
		 */
		EClass ROOM_INTERFACE = eINSTANCE.getRoomInterface();

		/**
		 * The meta object literal for the '<em><b>Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_INTERFACE__INSTANCE = eINSTANCE.getRoomInterface_Instance();

		/**
		 * The meta object literal for the '{@link Hotel.Room.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Room.impl.RoomImpl
		 * @see Hotel.Room.impl.RoomPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Room ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ROOM_ID = eINSTANCE.getRoom_RoomID();

		/**
		 * The meta object literal for the '<em><b>Room Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__ROOM_TYPE = eINSTANCE.getRoom_RoomType();

		/**
		 * The meta object literal for the '<em><b>Room Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ROOM_STATUS = eINSTANCE.getRoom_RoomStatus();

		/**
		 * The meta object literal for the '{@link Hotel.Room.RoomStatus <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Room.RoomStatus
		 * @see Hotel.Room.impl.RoomPackageImpl#getRoomStatus()
		 * @generated
		 */
		EEnum ROOM_STATUS = eINSTANCE.getRoomStatus();

	}

} //RoomPackage
