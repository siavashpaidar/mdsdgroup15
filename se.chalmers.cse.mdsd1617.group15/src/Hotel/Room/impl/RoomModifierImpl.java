/**
 */
package Hotel.Room.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Room.Room;
import Hotel.Room.RoomModifier;
import Hotel.Room.RoomPackage;
import Hotel.Room.RoomStatus;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Modifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RoomModifierImpl extends MinimalEObjectImpl.Container implements RoomModifier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomModifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_MODIFIER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addRoom(int roomNumber, String roomTypeDescription) {
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription(roomTypeDescription);
		if(roomType != null){
			EList<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
			Room room = new RoomImpl();
			room.setRoomID(roomNumber);
			room.setRoomStatus(RoomStatus.FREE);
			room.setRoomType(roomType);
			rooms.add(room);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void blockRoom(int roomID) {
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomID);
		if(room != null){
			room.setRoomStatus(RoomStatus.BLOCKED);
		} else {
			System.out.println("A room with ID " + roomID + " does not exist!");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeRoom(int roomID) {
		EList<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomID);
		if(room != null){
			rooms.remove(room);
		} else {
			System.out.println("A room with ID " + roomID + " does not exist!");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unblockRoom(int roomID) {
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomID);
		if(room != null){
			room.setRoomStatus(RoomStatus.FREE);
		} else {
			System.out.println("A room with ID " + roomID + " does not exist!");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_MODIFIER___ADD_ROOM__INT_STRING:
				addRoom((Integer)arguments.get(0), (String)arguments.get(1));
				return null;
			case RoomPackage.ROOM_MODIFIER___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
			case RoomPackage.ROOM_MODIFIER___REMOVE_ROOM__INT:
				removeRoom((Integer)arguments.get(0));
				return null;
			case RoomPackage.ROOM_MODIFIER___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomModifierImpl
