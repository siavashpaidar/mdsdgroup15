/**
 */
package Hotel.Room.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Room.Room;
import Hotel.Room.RoomLister;
import Hotel.Room.RoomPackage;
import Hotel.Room.RoomStatus;
import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lister</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RoomListerImpl extends MinimalEObjectImpl.Container implements RoomLister {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomListerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_LISTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> listOccupiedRooms(Date day) {
		EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
		EList<Room> occupiedRooms = new BasicEList<Room>();
		
		for(Booking booking : bookings){
			//Only if the booking covers the provided day
			if((booking.getFromDay().before(day) || booking.getFromDay().equals(day)) && (booking.getToDay().after(day) || booking.getToDay().equals(day))){
				EList<Room> rooms = booking.getRooms();
				for(Room room : rooms){
					if(room.getRoomStatus().equals(RoomStatus.OCCUPIED)){
						occupiedRooms.add(room);
					}
				}
			}
		}
		
		return occupiedRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<RoomType, EList<Room>> searchFreeRooms(Date fromDay, Date toDay, int minNumberOfBeds) {
		EMap<RoomType,EList<Room>> freeRooms = new BasicEMap<RoomType,EList<Room>>();
		if(fromDay.before(toDay)){
			EList<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
			EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
			
			for(Booking booking : bookings){ // Loop through all bookings
				if((fromDay.before(booking.getToDay()) || fromDay.equals(booking.getToDay())) && (booking.getFromDay().before(toDay) || booking.getToDay().equals(toDay))) { // For each booking that occurs(sometime) during the specified time interval
					// Remove the room(s) of that booking from the list
					EList<Room> bookingRooms = booking.getRooms();
					for(Room room : bookingRooms) {
						rooms.remove(room);
					}
				}
			}
					
			// Map the room types to the correct rooms
			for(Room room : rooms) { // Loop through all free rooms
				if(room.getRoomType().getNumberOfBeds() >= minNumberOfBeds) { // For each room that has the right amount of beds
					try { // If that roomtype exists in freeRooms
						freeRooms.get(room.getRoomType()).add(room); // Add the room to the mapentry in that roomtype
					} catch(Exception e) { // Else
						// Add that roomtype with a new list with this room in a new list
						EList<Room> value = new BasicEList<Room>();
						value.add(room);
						freeRooms.put(room.getRoomType(), value);
					}
				}
			}	
		}
		return freeRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_LISTER___LIST_OCCUPIED_ROOMS__DATE:
				return listOccupiedRooms((Date)arguments.get(0));
			case RoomPackage.ROOM_LISTER___SEARCH_FREE_ROOMS__DATE_DATE_INT:
				return searchFreeRooms((Date)arguments.get(0), (Date)arguments.get(1), (Integer)arguments.get(2));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomListerImpl
