/**
 */
package Hotel.Room;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.Room.RoomPackage#getRoomModifier()
 * @model
 * @generated
 */
public interface RoomModifier extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNumberRequired="true" roomNumberOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	void addRoom(int roomNumber, String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	void blockRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	void removeRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	void unblockRoom(int roomID);

} // RoomModifier
